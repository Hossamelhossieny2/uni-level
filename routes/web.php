<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\QrCodeController;
use App\Http\Controllers\CaptchaValidationController;
use App\Http\Controllers\JoinUserController;
use App\Http\Controllers\FinanceController;
use App\Http\Livewire\Genealogy;
use App\Http\Livewire\Memberlist;
use App\Http\Controllers\LocationController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::group([
  'prefix' => '{locale}',
  'where' => ['locale' => '[a-zA-Z]{2}'],
  'middleware' => 'setlocale'], function() {

    Route::get('contact-form-captcha', [CaptchaValidationController::class, 'index']);
    Route::post('captcha-validation', [CaptchaValidationController::class, 'capthcaFormValidate']);
    Route::get('reload-captcha', [CaptchaValidationController::class, 'reloadCaptcha']);

    Route::get('/', [HomeController::class,'index'])->name('website');
    Route::get('/generate-qrcode', [QrCodeController::class, 'index']);
    Route::get('newUserJoin/{token}/{pkg}',[JoinUserController::class,'join_app'])->name('newUserJoin');
    Route::post('storejoinApp',[JoinUserController::class,'storejoinApp'])->name('storejoinApp');
    Route::get('show-user-location-data', [LocationController::class, 'index']);
    Route::get('details', function () {

  $ip = Request::ip();
    $data = \Location::get($ip);
    dd($data);

});


    Route::middleware(['auth:sanctum', 'verified'])->group(function(){

        Route::get('dashboard',[HomeController::class,'dashboard'])->name('dashboard');
        Route::get('mydash',[HomeController::class,'mydash'])->name('mydash');
        Route::get('mytree', Genealogy::class)->name('mytree');
        Route::get('member_list', Memberlist::class)->name('member_list');
        Route::get('transpass',[FinanceController::class,'trans_pass'])->name('transpass');
        Route::Post('add_trans_pass',[FinanceController::class,'add_trans_pass'])->name('add_trans_pass');
        Route::get('charge_request',[FinanceController::class,'charge_request'])->name('charge_request');
        Route::get('my_wallet',[FinanceController::class,'my_wallet'])->name('my_wallet');
        Route::get('my_trans',[FinanceController::class,'my_trans'])->name('my_trans');
        Route::get('my_revenu',[FinanceController::class,'my_revenu'])->name('my_revenu');
        Route::get('user_trans',[FinanceController::class,'user_trans'])->name('user_trans');
        Route::post('do_user_trans',[FinanceController::class,'do_user_trans'])->name('do_user_trans');
        Route::post('add_address',[FinanceController::class,'add_address'])->name('add_address');
        Route::get('/delete_address/{id}',[FinanceController::class,'delete_address'])->name('delete_address');
        Route::post('add_charge_request',[FinanceController::class,'add_charge_request'])->name('add_charge_request');
        Route::get('/cancel_request/{id}',[FinanceController::class,'cancel_request'])->name('cancel_request');
        Route::post('do_charge',[FinanceController::class,'do_charge'])->name('do_charge');
        Route::any('/buy_pkg/{id}',[FinanceController::class,'buy_pkg'])->name('buy_pkg');
        // Route::post('/buy_pkg/{id}',[FinanceController::class,'buy_pkg'])->name('buy_pkg');
        Route::post('buy_invest',[FinanceController::class,'buy_invest'])->name('buy_invest');
        Route::get('/moveToBal/{id?}',[FinanceController::class,'moveToBal'])->name('moveToBal');
        Route::get('withdraw_request',[FinanceController::class,'withdraw_request'])->name('withdraw_request');
        Route::post('add_withdraw_request',[FinanceController::class,'add_withdraw_request'])->name('add_withdraw_request');
        Route::get('/cancel_w_request/{id}',[FinanceController::class,'cancel_w_request'])->name('cancel_w_request');
        Route::post('do_withdraw',[FinanceController::class,'do_withdraw'])->name('do_withdraw');

        Route::get('addlocation/{token}',[HomeController::class,'addlocation'])->name('addlocation');
        Route::get('mywallet', [HomeController::class,'my_wallet'])->name('mywallet');

        Route::get('invest_calculator', [HomeController::class,'invest_calculator'])->name('invest_calculator');
        Route::get('invest_plan/{id?}', [HomeController::class,'invest_plan'])->name('invest_plan');
        Route::get('my_invests', [HomeController::class,'my_invests'])->name('my_invests');

        Route::get('accept_charge', [AdminController::class,'accept_charge'])->name('accept_charge');
        Route::get('accept_withdraw', [AdminController::class,'accept_withdraw'])->name('accept_withdraw');
        Route::get('admin_dash', [AdminController::class,'index'])->name('admin_dash');

        Route::any('change_words', [AdminController::class,'change_words'])->name('change_words');
        Route::any('add_roadmap', [AdminController::class,'add_roadmap'])->name('add_roadmap');
        Route::get('del_roadmap/{id}', [AdminController::class,'del_roadmap'])->name('del_roadmap');
        Route::any('add_docs', [AdminController::class,'add_docs'])->name('add_docs');
        Route::any('change_pkgs/{id}', [AdminController::class,'change_pkgs'])->name('change_pkgs');
        Route::any('change_invest_plans/{id}', [AdminController::class,'change_invest_plans'])->name('change_invest_plans');
    });
});
