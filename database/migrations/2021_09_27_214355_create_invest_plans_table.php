<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invest_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country');
            $table->string('title');
            $table->string('title_small');
            $table->string('currency');
            $table->string('sign');
            $table->text('desc');
            $table->string('banner');
            $table->string('image');
            $table->string('volume');
            $table->decimal('change',4,2);
            $table->string('block');
            $table->integer('members');
            $table->integer('invest_cap');
            $table->string('status')->default('active');
            $table->date('start');
            $table->date('end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invest_plans');
    }
}
