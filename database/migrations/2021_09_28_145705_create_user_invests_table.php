<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInvestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_invests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('invest_id');
            $table->string('flag');
            $table->integer('amount');
            $table->integer('zoms_rate');
            $table->date('zoms_rate_end');
            $table->integer('pkg_rate');
            $table->timestamp('round_end');
            $table->timestamp('next_round')->nullable();
            $table->string('gain_round')->nullable();
            $table->string('gain_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_invests');
    }
}
