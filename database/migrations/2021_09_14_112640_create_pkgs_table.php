<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePkgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkgs', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('price')->default(0);
            $table->integer('max_out')->default(0);
            $table->integer('invest')->default(0);
            $table->integer('invest_count')->default(0);
            $table->integer('bvp')->default(0);
            $table->integer('direct')->default(0);
            $table->integer('pair')->default(0);
            $table->string('color')->nullable();
            $table->string('image')->nullable();
            $table->enum('support', [1, 0])->default(0);
            $table->enum('active', [1, 0])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkgs');
    }
}
