<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_charges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('charge')->nullable();
            $table->date('charge_date')->nullable();
            $table->string('wallet')->nullable();
            $table->string('email')->nullable();
            $table->decimal('amount', 8, 2)->default(0);
            $table->enum('accepted', ['0', '1'])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_charges');
    }
}
