<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('email')->unique();
            $table->string('phone_number')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->integer('position')->nullable();
            $table->enum('location', ['r', 'l'])->nullable();
            $table->integer('pkg')->default(0);
            $table->enum('pkg_active', ['1', '0'])->default(0);
            $table->integer('direct')->nullable();
            $table->integer('l')->default(0);
            $table->integer('bvl')->default(0);
            $table->integer('bvl_removed')->default(0);
            $table->integer('r')->default(0);
            $table->integer('bvr')->default(0);
            $table->integer('bvr_removed')->default(0);
            $table->foreignId('country_id')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('password');
            $table->string('trans_pass')->nullable();
            $table->string('profile_photo_path', 2048)->nullable();
            $table->enum('is_admin', ['0', '1'])->default(0);
            $table->enum('active', ['1', '0'])->default(1);
            $table->date('sub_end')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('api_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
