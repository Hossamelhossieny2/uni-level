<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Token;

class TokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Token::create( [
            'tokenable_type'=>'App\Models\User',
            'tokenable_id'=>1,
            'name'=>'l',
            'token'=>'6c6b88c5148760962e3d065b5181f7d1c542f205579b4f516d865edf7d920280',
            'abilities'=>'["read"]',
        ] );

        Token::create( [
            'tokenable_type'=>'App\Models\User',
            'tokenable_id'=>1,
            'name'=>'r',
            'token'=>'ce5eb1f02d9ddef845d2944c0be75aefd02bd413700dcca22fce8c5c33fc91c7',
            'abilities'=>'["read"]',
        ] );

    }
}