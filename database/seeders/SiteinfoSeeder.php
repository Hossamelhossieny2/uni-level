<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SiteInfo;

class SiteinfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteInfo::create( [
            'wait'=>45,
            'day1'=>16,
            'day2'=>1,
        ] );

    }
}