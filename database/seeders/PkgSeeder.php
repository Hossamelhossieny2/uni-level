<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pkg;

class PkgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pkg::create( [
            'name'=>'Starter',
            'price'=>100,
            'invest'=>100,
            'invest_count'=>5,
            'bvp'=>100,
            'direct'=>10,
            'pair'=>15,
            'max_out'=>100,
            'color'=>"#83A5F7",
            'active'=>1,
            'support'=>1
        ] );

        Pkg::create( [
            'name'=>'Professional',
            'price'=>500,
            'invest'=>1000,
            'invest_count'=>10,
            'bvp'=>600,
            'direct'=>60,
            'pair'=>90,
            'max_out'=>600,
            'color'=>"#49AA4D",
            'active'=>1,
            'support'=>1
        ] );

        Pkg::create( [
            'name'=>'Unlimited',
            'price'=>1000,
            'invest'=>10000,
            'invest_count'=>100,
            'bvp'=>1300,
            'direct'=>125,
            'pair'=>150,
            'max_out'=>1300,
            'color'=>"#C00337",
            'active'=>1,
            'support'=>1
        ] );

    }
}