<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\HomeSite;
use App\Models\Docs;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HomeSite::create( [
            'word'=>'title',
            'value'=>'Cyanus | ICO Crypto - ICO Landing Page'
        ] );

         HomeSite::create( [
            'word'=>'bannerT',
            'value'=>'Simple. Faster. Secure'
        ] );

         HomeSite::create( [
            'word'=>'bannerD',
            'value'=>'Distributing finance for everyone Buy Bitcoin and cryptocurrencies. Pay with Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        ] );

         HomeSite::create( [
            'word'=>'bannerB1',
            'value'=>'White Paper'
        ] );

         HomeSite::create( [
            'word'=>'bannerB2',
            'value'=>'One Pager'
        ] );

          HomeSite::create( [
            'word'=>'roadT',
            'value'=>'Roadmap'
        ] );

         HomeSite::create( [
            'word'=>'roadD',
            'value'=>'Our team working hardly to make archive lorem ipsum dolor sit amet, consectetur  amet, consectetur adipiscing elit.'
        ] );

           HomeSite::create( [
            'word'=>'docsT',
            'value'=>'Documents'
        ] );

         HomeSite::create( [
            'word'=>'docsD',
            'value'=>'Download the whitepaper and learn about ICO Token, the unique ICO Crypto approach and the team/advisors.'
        ] );

            HomeSite::create( [
            'word'=>'contactT',
            'value'=>'Contact Us'
        ] );

         HomeSite::create( [
            'word'=>'contactD',
            'value'=>'We are always open and we welcome and questions you have for our team. If you wish to get in touch, please fill out the form below. Someone from our team will get back to you shortly.'
        ] );

             HomeSite::create( [
            'word'=>'mobile',
            'value'=>'+44 0123 4567'
        ] );

         HomeSite::create( [
            'word'=>'mail',
            'value'=>'info@greenway.com'
        ] );

          HomeSite::create( [
            'word'=>'site_name',
            'value'=>'ICO Crypto'
        ] );

         HomeSite::create( [
            'word'=>'blockT',
            'value'=>'Decentralize Platform'
        ] );

          HomeSite::create( [
            'word'=>'blockD',
            'value'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore'
        ] );


        Docs::create( [
            'name'=>'White Paper',
            'image'=>'doc-a.jpg',
            'year'=>'2019'
        ] );

        Docs::create( [
            'name'=>'Two Pager',
            'image'=>'doc-b.jpg',
            'year'=>'2018'
        ] );

        Docs::create( [
            'name'=>'One Pager',
            'image'=>'doc-c.jpg',
            'year'=>'2018'
        ] );

        Docs::create( [
            'name'=>'Presentation',
            'image'=>'doc-d.jpg',
            'year'=>'2017'
        ] );

    }

}