<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create( [
            'user_id'=>1,
            'charge'=>'1',
            'add'=>'1',
            'edit'=>'1',
            'delete'=>'1',
            'balance'=>100000,
        ] );
    }
}