<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\InvestPlan;

class InvestplanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InvestPlan::create( [
        'id'=>1,
        'country'=>79,
        'title'=>'gorgia BTC farm',
        'title_small'=>'mining',
        'currency'=>'BTC',
        'sign'=>'BTC',
        'desc'=>'gorgia BTC farm For more details ',
        'banner'=>'img/uploads/bitcoin_farm_ge.jpg',
        'image'=>'img/uploads/bitcoin_farm_ge.jpg',
        'volume'=>'1000000',
        'change'=>'12.12',
        'block'=>'red',
        'members'=>11220,
        'invest_cap'=>10000000,
        'status'=>'active',
        'start'=>'2021-12-12',
        'end'=>'2025-02-21',
        'created_at'=>NULL,
        'updated_at'=>NULL
        ] );
                    
        InvestPlan::create( [
        'id'=>2,
        'country'=>226,
        'title'=>'UK BTC farm',
        'title_small'=>'mining',
        'currency'=>'BTC',
        'sign'=>'BTC',
        'desc'=>'UK BTC farm for more details',
        'banner'=>'img/uploads/bitcoin_farm_uk.jpg',
        'image'=>'img/uploads/bitcoin_farm_uk.jpg',
        'volume'=>'1200000',
        'change'=>'82.12',
        'block'=>'red',
        'members'=>12023,
        'invest_cap'=>10000000,
        'status'=>'active',
        'start'=>'2022-12-01',
        'end'=>'2023-12-31',
        'created_at'=>NULL,
        'updated_at'=>NULL
        ] );
                    
        InvestPlan::create( [
        'id'=>3,
        'country'=>79,
        'title'=>'GE ETH farm',
        'title_small'=>'mining',
        'currency'=>'ETH',
        'sign'=>'ETH',
        'desc'=>'gorgia ETHERUIM farm for more details',
        'banner'=>'img/uploads/ethereum_farm_ge.jpg',
        'image'=>'img/uploads/ethereum_farm_ge.jpg',
        'volume'=>'1450000',
        'change'=>'52.12',
        'block'=>'red',
        'members'=>1723,
        'invest_cap'=>10000000,
        'status'=>'active',
        'start'=>'2022-12-01',
        'end'=>'2024-02-28',
        'created_at'=>NULL,
        'updated_at'=>NULL
        ] );
                    
        InvestPlan::create( [
        'id'=>4,
        'country'=>177,
        'title'=>'RU ETH farm',
        'title_small'=>'mining',
        'currency'=>'ETH',
        'sign'=>'ETH',
        'desc'=>'Russian ETHERUIM farm more details',
        'banner'=>'img/uploads/ethereum_farm_ru.jpg',
        'image'=>'img/uploads/ethereum_farm_ru.jpg',
        'volume'=>'2000000',
        'change'=>'72.12',
        'block'=>'red',
        'members'=>123,
        'invest_cap'=>10000000,
        'status'=>'active',
        'start'=>'2022-12-01',
        'end'=>'2024-02-28',
        'created_at'=>NULL,
        'updated_at'=>NULL
        ] );
    }
}
