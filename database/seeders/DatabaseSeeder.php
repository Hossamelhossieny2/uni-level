<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(PkgSeeder::class);
        $this->call(InvestplanSeeder::class);
        $this->call(TokenSeeder::class);
        $this->call(SiteinfoSeeder::class);
        $this->call(UserbalanceSeeder::class);
        $this->call(HomeSeeder::class);
    }
}
