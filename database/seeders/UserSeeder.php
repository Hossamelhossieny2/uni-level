<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create( [
            'username'=>'company',
            'email'=>'company@mail.com',
            'gender'=>'male',
            'position'=>0,
            'pkg'=>3,
            'pkg_active'=>1,
            'direct'=>1,
            'country_id'=>177,
            'profile_photo_path'=>'img/default/user_zoms.png',
            'active'=>'1',
            'is_admin'=>'1',
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'dido1',
            'email'=>'dido1@gmail.com',
            'gender'=>'male',
            'country_id'=>79,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'dido2',
            'email'=>'dido2@gmail.com',
            'gender'=>'male',
            'country_id'=>79,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'dido3',
            'email'=>'dido3@gmail.com',
            'gender'=>'male',
            'country_id'=>79,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'dido4',
            'email'=>'dido4@gmail.com',
            'gender'=>'male',
            'country_id'=>79,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'dido5',
            'email'=>'dido5@gmail.com',
            'gender'=>'male',
            'country_id'=>79,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'hoho1',
            'email'=>'hoho1@gmail.com',
            'gender'=>'male',
            'country_id'=>63,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'hoho2',
            'email'=>'hoho2@gmail.com',
            'gender'=>'male',
            'country_id'=>63,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'hoho3',
            'email'=>'hoho3@gmail.com',
            'gender'=>'male',
            'country_id'=>63,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'hoho4',
            'email'=>'hoho4@gmail.com',
            'gender'=>'male',
            'country_id'=>63,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'hoho5',
            'email'=>'hoho5@gmail.com',
            'gender'=>'male',
            'country_id'=>63,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'abdo1',
            'email'=>'abdo1@gmail.com',
            'gender'=>'male',
            'country_id'=>226,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'abdo2',
            'email'=>'abdo2@gmail.com',
            'gender'=>'male',
            'country_id'=>226,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'abdo3',
            'email'=>'abdo3@gmail.com',
            'gender'=>'male',
            'country_id'=>226,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'abdo4',
            'email'=>'abdo4@gmail.com',
            'gender'=>'male',
            'country_id'=>226,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );

        User::create( [
            'username'=>'abdo5',
            'email'=>'abdo5@gmail.com',
            'gender'=>'male',
            'country_id'=>226,
            'password'=>Hash::make('123123'),
            'email_verified_at' => Carbon::now(),
        ] );
    }
}