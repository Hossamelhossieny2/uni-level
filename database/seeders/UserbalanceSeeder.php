<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserBalance;

class UserbalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserBalance::create( [
            'user_id'=>1,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>2,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>3,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>4,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>5,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>6,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>7,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>8,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>9,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>10,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>11,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>12,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>13,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>14,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>15,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

        UserBalance::create( [
            'user_id'=>16,
            'balance'=>0,
            'direct'=>0,
            'network'=>0,
            'transfer'=>0,
        ] );

    }
}