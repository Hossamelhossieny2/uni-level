<?php

namespace App\Http\Livewire;
use App\Models\User;

use Livewire\Component;

class Genealogy extends Component
{
    public $mainUser;
    public $tree;

    public function mount()
    {
        $grand = $this->mainUser = User::where('id',auth()->id())->with('country')->with('pkgs')->first();
        $this->child($grand->id);
    }

    public function child($person)
    {
        $leftuser = User::where('position',$person)->where('location','l')->with('country')->with('pkgs')->first();
        if(!empty($leftuser->id)){
            $this->tree[$person]['l'] = $leftuser;
            $this->child($leftuser->id);
        }

        $rightuser = User::where('position',$person)->where('location','r')->with('country')->with('pkgs')->first();
        if(!empty($rightuser->id)){
            $this->tree[$person]['r'] = $rightuser;
            $this->child($rightuser->id);
        }
    }

    public function render()
    {

        return view('livewire.genealogy');
    }
}
