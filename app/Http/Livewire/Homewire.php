<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Homewire extends Component
{
    public function render()
    {
        return view('livewire.homewire');
    }
}
