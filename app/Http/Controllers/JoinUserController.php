<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Laravel\Jetstream\Jetstream;
use App\Models\User;
use App\Models\Token;
use App\Models\Pkg;
use App\Models\Country;
use App\Models\UserBalance;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Transaction;
use DB;

class JoinUserController extends Controller
{
    use HasApiTokens;

    public $appline;

    // public function join_app($lang='',$token,$pkg)
    // {
        
        
    //    $arr = ['usertoken'=>$token,'pkg'=>$pkg];

    //     return view('join.joinapp',$arr);
    // }

    public function storejoinApp(Request $request)
    {
        $rules =[
            'usertoken'  => 'required',
            'trans_pass'     => 'required',
        ];
        $this->validate($request, $rules);

        $user = User::find(auth()->id());
        if (!Hash::check($request['trans_pass'], $user->trans_pass)) {
            return back()->with('error','wrong Transaction Password !');
        }

        $userIp = request()->ip();
        if($userIp == "false" || $userIp == "127.0.0.1")$userIp='41.46.213.47';
        $locationData = \Location::get($userIp);
        if(!empty($locationData)){
            $countryCode = $locationData->countryCode;
        }else{
            $countryCode = 'EG';
        }
        $user_country = Country::where('iso',$countryCode)->first();

        $check_token = Token::where('token',$request['usertoken'])->first();
        
        if(!isset($check_token))
        {
            return back()->with('error','unknown This appline Token !');
        }
        $appline = $check_token->tokenable_id;
        $location = $check_token->name;
        $this_pkg = Pkg::find($request['pkg']);
        $newpos = $this->checkUserLocation($appline,$location);
        
        $useradd            = User::find(auth()->id());
        //dd($this->appline);
        $useradd->position  = $this->appline;
        $useradd->location  = $location;
        $useradd->direct    = $appline;
        $useradd->pkg       = $this_pkg->id;
        $useradd->sub_end   = date('Y-m-d', strtotime("+25 months", time()));
        $useradd->pkg_active       = 1;
        $useradd->country_id = $user_country->id;
        $useradd->save();

        // $userBalance = new UserBalance;
        // $userBalance->user_id = $useradd->id;
        // $userBalance->save();

        $useradd->createToken('r', ['register:right'])->plainTextToken;
        $useradd->createToken('l', ['register:left'])->plainTextToken;
        
        $this->add_user_count($this->appline,$location,$this_pkg->bvp);

        $this->add_appline_direct($appline,$this_pkg->direct,$this_pkg->price);

        //return redirect()->guest('login');
        return redirect()->route('mytree',app()->getLocale());
    }

    public function checkUserLocation($appline,$location)
    {
        $ifUser = User::where('position',$appline)->where('location',$location)->first();
        if(isset($ifUser)){
            $this->checkUserLocation($ifUser->id,$location);
        }else{
            $this->appline = $appline;
        }
    }

    public function add_user_count($appline,$location,$bvp)
    {
        $this_appline = User::find($appline);
        User::where('id', $appline)->update([ $location=> DB::raw($location.'+1') , 'bv'.$location=>DB::raw('bv'.$location.'+'.$bvp) ]);

        if(!empty($this_appline->position)){
            $this->add_user_count($this_appline->position,$this_appline->location,$bvp);
        }else{
            return;
        }
    }

    public function add_appline_direct($appline,$direct,$price)
    {
        $applineBalance = UserBalance::where('user_id',$appline)->first();
        $applineBalance->direct = $applineBalance->direct + $direct;
        $applineBalance->save();

        $trans = new Transaction;
        $trans->user_id = $appline;
        $trans->amount = $direct;
        $trans->trans = 'in';
        $trans->desc = 'Direct added form user '.auth()->user()->username;
        $trans->type = 'site';
        $trans->save();

        $userBalance = UserBalance::where('user_id',auth()->id())->first();
        
        $userBalance->balance = $userBalance->balance - $price;
        $userBalance->save();

        $trans = new Transaction;
        $trans->user_id = auth()->id();
        $trans->amount = $price;
        $trans->trans = 'out';
        $trans->desc = 'buy package from my Balance';
        $trans->type = 'site';
        $trans->save();
    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}
