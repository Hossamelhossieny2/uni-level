<?php

namespace App\Http\Controllers;
use App\Models\InvestPlan;
use App\Models\Pkg;
use App\Models\UserCharge;
use App\Models\UserWithdraw;
use App\Models\Admin;
use App\Models\HomeSite;
use App\Models\RoadMap;
use App\Models\Country;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['admin'] = Admin::where('user_id',auth()->id())->first();
        $arr['charge'] = UserCharge::where('charge',auth()->id())->sum('amount');
        $arr['charge_number'] = UserCharge::where('charge',auth()->id())->count();
        return view('admin.admin_dash',$arr);
    }

    public function accept_charge()
    {
        $arr['admin'] = Admin::where('user_id',auth()->id())->first();
        $arr['requests'] = UserCharge::orderBy('id','desc')->with('user')->get();
        //dd($arr['requests']);
        return view('admin.accept_charge',$arr);
    }

     public function accept_withdraw()
    {
        $arr['admin'] = Admin::where('user_id',auth()->id())->first();
        $arr['requests'] = UserWithdraw::orderBy('id','desc')->with('user')->get();
        //dd($arr['admin']);
        return view('admin.accept_withdraw',$arr);
    }
    
    public function change_words()
    {
        $arr['requests'] = HomeSite::get();
        if($_POST){
            //dd($_POST);
            $editword = HomeSite::where('word',$_POST['word'])->first();
            $editword->value = $_POST['replace'];
            $editword->save();
            return redirect()->route('change_words',app()->getLocale())->with('success','changes done successfully ..');
        }
        return view('admin.change_words',$arr);
    }

    public function add_roadmap()
    {
        $arr['requests'] = RoadMap::orderBy('sort','desc')->get();
        if($_POST){
            $addRoadMap = new RoadMap;
            $addRoadMap->dat = $_POST['date'];
            $addRoadMap->tit = $_POST['word'];
            $addRoadMap->des = $_POST['val'];
            $addRoadMap->sort = $_POST['sort'];
            $addRoadMap->save();
            return redirect()->route('add_roadmap',app()->getLocale())->with('success','changes done successfully ..');
        }
        return view('admin.add_roadmap',$arr);
    }

    public function del_roadmap($lang,$id)
    {
       RoadMap::where('id',$id)->delete();
        
        return redirect()->route('add_roadmap',app()->getLocale())->with('success','changes done successfully ..');
        
    }

    public function change_pkgs($lang,$id)
    {
        $arr['requests'] = Pkg::get();
        $arr['pkg'] = Pkg::where('id',$id)->first();
        if($_POST){
            $editpkg = Pkg::where('id',$_POST['id'])->first();
            $editpkg->name = $_POST['name'];
            $editpkg->price = $_POST['price'];
            $editpkg->max_out = $_POST['max_out'];
            $editpkg->invest = $_POST['invest'];
            $editpkg->invest_count = $_POST['invest_count'];
            $editpkg->bvp = $_POST['bvp'];
            $editpkg->direct = $_POST['direct'];
            $editpkg->pair = $_POST['pair'];
            $editpkg->color = $_POST['color'];
            $editpkg->active = $_POST['active'];
            $editpkg->save();

            return redirect()->route('change_pkgs',[app()->getLocale(),$_POST['id']])->with('success','changes done successfully ..');
        }
        return view('admin.add_pkgs',$arr);
    }

    public function change_invest_plans(Request $request,$lang,$id)
    {
        $arr['requests'] = InvestPlan::get();
        $arr['plan'] = $this_plan = InvestPlan::where('id',$id)->first();
        $arr['country'] = Country::all();

        if($_POST){
            if($request->hasFile('image')){
                $imageName = time().'.'.$request->image->extension();  
                $request->image->move(public_path('img/uploads'), $imageName);
                $file = 'img/uploads/'.$imageName;
            }else{
                $file = $this_plan['image'];
            }

            $editinvest = InvestPlan::where('id',$_POST['id'])->first();
            $editinvest->title = $_POST['title'];
            $editinvest->title_small = $_POST['title_small'];
            $editinvest->currency = $_POST['currency'];
            $editinvest->sign = $_POST['sign'];
            $editinvest->desc = $_POST['desc'];
            $editinvest->volume = $_POST['volume'];
            $editinvest->members = $_POST['members'];
            $editinvest->invest_cap = $_POST['invest_cap'];
            $editinvest->country = $_POST['country'];
            $editinvest->start = $_POST['start'];
            $editinvest->end = $_POST['end'];
            $editinvest->status = $_POST['status'];
            $editinvest->image = $file;
            $editinvest->save();

            return redirect()->route('change_invest_plans',[app()->getLocale(),$_POST['id']])->with('success','changes done successfully ..');
        }
        return view('admin.add_plans',$arr);
    }
}
