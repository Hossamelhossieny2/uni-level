<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pkg;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\CryptoRate;
use App\Models\HomeSite;
use App\Models\Docs;
use App\Models\RoadMap;
use Carbon\Carbon;
use App\Models\InvestPlan;

class HomeController extends Controller
{

    public function index()
    {
        $pag = [];

        $site = HomeSite::all();

        foreach($site as $s){
            $pag[$s->word] = $s->value;
        }
        $arr['page'] = $pag;
        $arr['docs'] = Docs::all();
        $arr['roadmap'] = RoadMap::get();

        return view('frontside',$arr);
    }

    public function dashboard()
    {
        $test = CryptoRate::OrderBy('id','desc')->first();

        if(!$test || $test->get_time < Carbon::now()->subHour(2)){
            $urlContents = file_get_contents('http://api.coinlayer.com/api/live?access_key=15ab5413445d9414fbf5fcb5a712486f');
            $jsonData = json_decode($urlContents, true);
            //dd($jsonData['rates']);
            $rate = new CryptoRate;
            $rate->api = json_encode($jsonData['rates']);
            $rate->get_time = Carbon::now();
            $rate->save();
        }else{
            $rate = $test;
        }

        $arr['rates'] = json_decode($rate->api);

        return view('user.dashboard',$arr);
    }

    public function mydash()
    {
        $arr['pkgs'] = Pkg::get();
        $arr['user'] = User::where('id',auth()->user()->id)->with(['balance','pkgs','appline','invests.plans'])->first();
        $arr['plans'] = InvestPlan::with('it_country')->get();
        //dd($arr['user']);
        return view('user.mydash',$arr);
    }

    public function my_invests()
    {
        $a = "";$d = "";$h = "";
        $arr['user'] = User::where('id',auth()->user()->id)->with(['balance','pkgs','appline','invests.plans'])->first();
        foreach($arr['user']['invests'] as $hh){
            $a .= $hh->id.',';
            //$dist_date = Carbon::parse($hh->next_round);
            //$hours = Carbon::now()->diffInHours($dist_date, false);
            $h .=strtotime($hh->next_round).',';
        }

        $arr['inv_ids'] = substr($a, 0, -1);
        $arr['inv_hrs'] = substr($h, 0, -1);

        return view('user.myinvest',$arr);
    }

    public function member_list()
    {
        return view('user.member_list');
    }

    public function genealogy($id)
    {
        return view('user.genealogy');
    }

    public function invest_calculator()
    {
        $arr['user'] = User::where('id',auth()->user()->id)->with(['balance','pkgs','appline'])->first();

        return view('invest.calculator',$arr);
    }

    public function invest_plan($lang,$id="")
    {
        $arr['plan'] = InvestPlan::where('id',$id)->with('it_country')->first();
        $arr['user'] = User::where('id',auth()->user()->id)->with(['balance','pkgs','appline'])->first();

        return view('invest.plan',$arr);
    }

    public function my_wallet()
    {
        return view('user.wallet');
    }
}
