<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Laravel\Jetstream\Jetstream;
use App\Models\User;
use App\Models\Token;
use App\Models\Pkg;
use App\Models\Country;
use App\Models\UserBalance;
use Laravel\Sanctum\HasApiTokens;
use DB;

class JoinUserController extends Controller
{
    use HasApiTokens;

    public $appline;

    public function join_app($lang='',$token,$pkg)
    {
        
        
       $arr = ['usertoken'=>$token,'pkg'=>$pkg];

        return view('join.joinapp',$arr);
    }

    public function storejoinApp(Request $request)
    {
        $rules =[
            'username'  => 'required|string|max:255|min:4|unique:users',
            'email'     => 'required|string|email|max:255',
            'password'  => 'required|confirmed|min:6',
            'terms'     => 'required:accepted',
        ];
        $this->validate($request, $rules);

        $userIp = request()->ip();
        if($userIp == "false" || $userIp == "127.0.0.1")$userIp='41.46.213.47';
        $locationData = \Location::get($userIp);
        $user_country = Country::where('iso',$locationData->countryCode)->first();

        $check_token = Token::where('token',$request['usertoken'])->first();
        
        // $appline = $check_token->tokenable_id;
        // $location = $check_token->name;
        // $this_pkg = Pkg::find($request['pkg']);
        // $newpos = $this->checkUserLocation($appline,$location);
        
        $useradd            = new User;
        $useradd->username  = $request['username'];
        $useradd->email     = $request['email'];
        $useradd->password  = Hash::make($request['password']);
        //$useradd->position  = $this->appline;
        //$useradd->location  = $location;
        //$useradd->direct    = $appline;
        //$useradd->pkg       = $this_pkg->id;
        $useradd->country_id = $user_country->id;
        $useradd->gender    = $request['gender'];
        $useradd->save();

        $userBalance = new UserBalance;
        $userBalance->user_id = $useradd->id;
        $userBalance->save();

        //$useradd->createToken('r', ['register:right'])->plainTextToken;
        //$useradd->createToken('l', ['register:left'])->plainTextToken;
        
        //$this->add_user_count($this->appline,$location,$this_pkg->bvp);

        return redirect()->guest('login');
    }

    public function checkUserLocation($appline,$location)
    {
        $ifUser = User::where('position',$appline)->where('location',$location)->first();
        if(isset($ifUser)){
            $this->checkUserLocation($ifUser->id,$location);
        }else{
            $this->appline = $appline;
        }
    }

    public function add_user_count($appline,$location,$bvp)
    {
        $this_appline = User::find($appline);
        User::where('id', $appline)->update([ $location=> DB::raw($location.'+1') , 'bv'.$location=>DB::raw('bv'.$location.'+'.$bvp) ]);

        if(!empty($this_appline->position)){
            $this->add_user_count($this_appline->position,$this_appline->location,$bvp);
        }else{
            return;
        }
    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}
