<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserWallet;
use App\Models\UserCharge;
use App\Models\UserWithdraw;
use App\Models\UserBalance;
use App\Models\Transaction;
use App\Models\Admin;
use App\Models\User;
use App\Models\Pkg;
use App\Models\UserInvest;
use App\Models\InvestPlan;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class FinanceController extends Controller
{
    public function trans_pass()
    {
        return view('user.trans_pass');
    }

    public function buy_invest(Request $request)
    {
        $this->validate($request, [
            'invest_amount' => 'numeric|min:250',
            'trans_pass'    => 'required|min:4',
        ]);

        $user = User::find(auth()->id());
        if (!Hash::check($request['trans_pass'], $user->trans_pass)) {
            return back()->with('error','wrong Transaction Password !');
        }

        $user_bal = UserBalance::where('user_id',auth()->id())->first();
        if($request['invest_amount'] > $user_bal->balance){
            return back()->with('error','Insufficient User balance !');
        }else{
            //dd($request['invest_amount']);
            $user_bal->balance = $user_bal->balance - $request['invest_amount'];
            $user_bal->save();

            $plan = InvestPlan::where('id',$request['invest_plan'])->with('it_country')->first();
            $plan->members = $plan->members + 1;
            $plan->save();

            $country_invest = Country::find($plan->country);
            
            $user_pkg = Pkg::find(auth()->user()->pkg);

            

            //for($i=0;$i<25;$i++){
            $profit = ($request['invest_amount'] + ($request['invest_amount'] * ($user_pkg->invest + $user_pkg->invest_count))/100)/25;
            $gain_profit = mt_rand(($profit-$profit*0.05)*100,($profit+$profit*0.09)*100)/100;
            //dd(auth()->user()->sub_end);
            $inv = new UserInvest;
            $inv->user_id = auth()->id();
            $inv->invest_id =$request['invest_plan'];
            $inv->amount =$request['invest_amount'];
            $inv->flag = $country_invest->iso;
            $inv->zoms_rate = $user_pkg->invest;
            $inv->pkg_rate = $user_pkg->invest_count;
            $inv->gain_round = $gain_profit;
            $inv->zoms_rate_end = auth()->user()->sub_end;
            $inv->round_end = Carbon::now()->addMonths(24);
            $inv->next_round = Carbon::now()->addMonths(1);
            $inv->save();

            $trans = new Transaction;
            $trans->user_id = auth()->id();
            $trans->amount = $request['invest_amount'];
            $trans->trans = 'out';
            $trans->desc = 'Join Invest in : '.$plan->title.' in '.$plan['it_country']['nicename'].' in ('.$country_invest->nicename.')';
            $trans->type = 'site';
            $trans->save();
        //}
            return back()->with('success','Invest Plan Added .. our system will replay within(1-3)Min !');
       }

    }

    public function add_trans_pass(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:4|confirmed',
        ]);

        $trans_pass = User::find(auth()->id());
        $trans_pass->trans_pass = Hash::make($request['password']);
        $trans_pass->save();

        // dd($trans_pass);
        return redirect()->route('transpass',app()->getLocale());
    }

    public function charge_request()
    {
        if(!empty(auth()->user()->trans_pass)){
            $arr['wallets'] = UserWallet::where('user_id',auth()->id())->get();
            $arr['requests'] = UserCharge::where('user_id',auth()->id())->get();
            
            return view('finance.charge_request',$arr);
        }else{
            return redirect()->route('transpass',app()->getLocale());
        }
    }

    public function withdraw_request()
    {
        if(!empty(auth()->user()->trans_pass)){
            $arr['wallets'] = UserWallet::where('user_id',auth()->id())->get();
            $arr['requests'] = UserWithdraw::where('user_id',auth()->id())->get();
            
            return view('finance.withdraw_request',$arr);
        }else{
            return redirect()->route('transpass',app()->getLocale());
        }
    }

    public function my_wallet()
    {

        if(!empty(auth()->user()->trans_pass)){

            $arr['wallets'] = UserWallet::where('user_id',auth()->id())->get();
            return view('wallet.my_wallet',$arr);
        }else{
            return redirect()->route('transpass',app()->getLocale());
        }
        
    }

    public function add_address(Request $request)
    {
        $this->validate($request, [
            'address' => 'required|min:26',
            'wallet_name' => 'required',
            'wallet_email' => 'required|email',
        ]);

        $user_wallet = new UserWallet;
        $user_wallet->user_id = auth()->id();
        $user_wallet->wallet_name = $request['wallet_name'];
        $user_wallet->wallet = $request['address'];
        $user_wallet->wallet_email = $request['wallet_email'];
        $user_wallet->type = 'usdt';
        $user_wallet->save();

        return back()->with('success','Address Added');
            
    }

    public function delete_address($lang,$id)
    {
        $wallet = UserWallet::find($id);
        $wallet->delete();
        return back()->with('success','Address deleted !');
    }

    public function cancel_request($lang,$id)
    {
        $wallet = UserCharge::find($id);
        $wallet->delete();
        return back()->with('success','request deleted !');
    }

     public function cancel_w_request($lang,$id)
    {
        $wallet = UserWithdraw::find($id);
        $amount = $wallet->amount;
        $wallet->delete();

        $user_bal = UserBalance::where('user_id',auth()->id())->first();
        $user_bal->pending = $user_bal->pending-$amount;
        $user_bal->balance = $user_bal->balance+$amount;
        $user_bal->save();

        $trans = new Transaction;
        $trans->user_id = auth()->id();
        $trans->amount = $amount;
        $trans->trans = 'in';
        $trans->desc = 'Back Requested amount ('.$amount.') From pending To your Balance';
        $trans->type = 'user';
        $trans->save();

        return back()->with('success','request deleted ! and Back '.$amount.' To your Balance ');
    }

    public function add_charge_request(Request $request)
    {
        $this->validate($request, [
            'wallet' => 'required',
            'email' => 'required|email',
            'amount' => 'required|numeric',
            'trans_pass' => 'required',
        ]);
        
        $user = User::find(auth()->id());
        if (!Hash::check($request['trans_pass'], $user->trans_pass)) {
            return back()->with('error','wrong Transaction Password !');
        }
        
        $charge_request = new UserCharge;
        $charge_request->user_id = auth()->id();
        $charge_request->wallet= $request['wallet'];
        $charge_request->email = $request['email'];
        $charge_request->amount = $request['amount'];
        $charge_request->save();
        return back()->with('success','Charge Request Added .. our system will replay within(1-3)hours !');
    }

    public function add_withdraw_request(Request $request)
    {
        $this->validate($request, [
            'wallet' => 'required',
            'amount' => 'required|numeric|min:250',
            'trans_pass' => 'required',
        ]);
        
        $user = User::find(auth()->id());
        if (!Hash::check($request['trans_pass'], $user->trans_pass)) {
            return back()->with('error','wrong Transaction Password !');
        }
        
        $user_bal = UserBalance::where('user_id',auth()->id())->first();
        if($request['amount'] > $user_bal->balance){
            return back()->with('error','Insufficient User balance !');
        }

        $user_bal->balance = $user_bal->balance - $request['amount'];
        $user_bal->pending = $user_bal->pending + $request['amount'];
        $user_bal->save();

        $withdraw_request = new UserWithdraw;
        $withdraw_request->user_id = auth()->id();
        $withdraw_request->wallet= $request['wallet'];
        $withdraw_request->amount = $request['amount'];
        $withdraw_request->save();

        $trans = new Transaction;
        $trans->user_id = auth()->id();
        $trans->amount = $request['amount'];
        $trans->trans = 'out';
        $trans->desc = 'Request Withdraw To Wallet ('.$request['wallet'].')';
        $trans->type = 'user';
        $trans->save();

        return back()->with('success','Withdraw Request Added .. our system will replay within(1-3)hours !');
    }

    public function do_charge(Request $request)
    {

        if(auth()->user()->password == unserialize($request['otp'])){
            $admin = Admin::where('user_id',auth()->id())->first();
            $charge = UserCharge::find($request['requested']);
            $user_balance = UserBalance::where('user_id',$charge->user_id)->first();

            if($admin->balance >= $charge->amount && $charge->accepted == 0){
                $admin->balance = $admin->balance - $charge->amount;
                $admin->save();

                $trans = new Transaction;
                $trans->user_id = auth()->id();
                $trans->trans = 'out';
                $trans->amount = $charge->amount;
                $trans->desc = 'accept charge for user id: '.$charge->user_id;
                $trans->type = 'admin';
                $trans->save();

                $charge->accepted = '1';
                $charge->charge = auth()->id();
                $charge->charge_date = Carbon::now();
                $charge->save();

                $user_balance->balance = $user_balance->balance + $charge->amount;
                $user_balance->save();

                $trans = new Transaction;
                $trans->user_id = $user_balance->user_id;
                $trans->amount = $charge->amount;
                $trans->trans = 'in';
                $trans->desc = 'accepted charge form systems ';
                $trans->type = 'site';
                $trans->save();

                return back()->with('success','request success !');
            }else{
                return back()->with('error','Insufficient admin balance !');
            }
        }else{
            return back()->with('error','unknown error !');
        }
    }

    public function do_withdraw(Request $request)
    {

        if(auth()->user()->password == unserialize($request['otp'])){
            $admin = Admin::where('user_id',auth()->id())->first();
            $charge = UserWithdraw::find($request['requested']);
            $user_balance = UserBalance::where('user_id',$charge->user_id)->first();

            if($admin->balance >= $charge->amount && $charge->accepted == 0){
                $admin->balance = $admin->balance + $charge->amount;
                $admin->save();

                $trans = new Transaction;
                $trans->user_id = auth()->id();
                $trans->trans = 'in';
                $trans->amount = $charge->amount;
                $trans->desc = 'accept withdraw for user id: '.$charge->user_id;
                $trans->type = 'admin';
                $trans->save();

                $charge->accepted = '1';
                $charge->charge = auth()->id();
                $charge->pay_time = Carbon::now();
                $charge->save();

                $user_balance->pending = $user_balance->pending - $charge->amount;
                $user_balance->save();

                $trans = new Transaction;
                $trans->user_id = $user_balance->user_id;
                $trans->amount = 0;
                $trans->trans = 'out';
                $trans->desc = 'accepted withdraw form systems For ('.$charge->amount.')';
                $trans->type = 'site';
                $trans->save();

                return back()->with('success','request success !');
            }else{
                return back()->with('error','Insufficient admin balance !');
            }
        }else{
            return back()->with('error','unknown error !');
        }
    }

    public function buy_pkg($lang,$pkg)
    {
        $arr['user'] = User::where('id',auth()->user()->id)->with('balance')->first();
        $arr['pkg'] = Pkg::find($pkg);
        $arr['wallets'] = UserWallet::where('user_id',auth()->id())->get();

        return view('user.buy_pkg',$arr);
    }

    public function my_trans()
    {
        if(!empty(auth()->user()->trans_pass)){
            $arr['trans'] = Transaction::where('user_id',auth()->id())->get();
            $arr['user'] = User::where('id',auth()->user()->id)->with('balance')->first();
            return view('finance.trans',$arr);
        }else{
            return redirect()->route('transpass',app()->getLocale());
        }
    }

    public function my_revenu()
    {
        if(!empty(auth()->user()->trans_pass)){
            $arr['trans'] = Transaction::where('user_id',auth()->id())->get();
            $arr['user'] = User::where('id',auth()->user()->id)->with('balance')->first();
            return view('finance.revenu',$arr);
        }else{
            return redirect()->route('transpass',app()->getLocale());
        }
    }

    public function user_trans()
    {
        if(!empty(auth()->user()->trans_pass)){
            $arr['trans'] = Transaction::where('user_id',auth()->id())->get();
            $arr['user'] = User::where('id',auth()->user()->id)->with('balance')->first();
            return view('finance.user_trans',$arr);
        }else{
            return redirect()->route('transpass',app()->getLocale());
        }
    }

    public function do_user_trans(Request $request)
    {
        $this->validate($request, [
            'amount'     => 'required|numeric',
            'trans_pass' => 'required',
            'user_email' => 'required|email',
        ]);

        $user = User::find(auth()->id());
        if (!Hash::check($request['trans_pass'], $user->trans_pass)) {
            return back()->with('error','wrong Transaction Password !');
        }

        $email = User::where('email',$request['user_email'])->first();
        if ($email === NULL) {
            return back()->with('error','wrong User Email !');
        }

        $amount = UserBalance::where('user_id',auth()->id())->first();
        if ($amount->balance < $request['amount']) {
            return back()->with('error','Insufficient User Balance !');
        }

        $amount->balance =  $amount->balance - $request['amount'];
        $amount->save();
       
       $trans = new Transaction;
        $trans->user_id = auth()->id();
        $trans->amount = $request['amount'];
        $trans->trans = 'out';
        $trans->desc = 'Transfer To user '.$email->username.' ('.$email->username.')';
        $trans->type = 'site';
        $trans->save();

        $touser = UserBalance::where('user_id',$email->id)->first();
        $touser->transfer = $touser->transfer + $request['amount'];
        $touser->save();

        $trans2 = new Transaction;
        $trans2->user_id = $email->id;
        $trans2->amount = $request['amount'];
        $trans2->trans = 'in';
        $trans2->desc = 'Transfer From user '.auth()->user()->username.' ('.auth()->user()->username.')';
        $trans2->type = 'user';
        $trans2->save();

        return back()->with('success','Transaction Done .. ');
            
    }

    public function moveToBal($lang,$from)
    {
        $userBal = UserBalance::find(auth()->id());
        $removed_bal = $userBal->$from;
        
        $userBal->$from = 0;
        $userBal->balance = $userBal->balance + $removed_bal;
        $userBal->save();

        $trans2 = new Transaction;
        $trans2->user_id = auth()->id();
        $trans2->amount = $removed_bal;
        $trans2->trans = 'in';
        $trans2->desc = 'Transfer From your '.$from.' account To your Balance ';
        $trans2->type = 'user';
        $trans2->save();

        return back()->with('success','Transaction Done .. ');
    }
}
