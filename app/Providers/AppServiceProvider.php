<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\InvestPlan;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
                $view->with('invest_plans', InvestPlan::with('it_country')->get());
        });
        
        Str::macro('money', function (float $num = null, int $decimals = 2, string $currency = 'LE', ?string $decimal_separator = '.', ?string $thousands_separator = ',') {
            return $num ? number_format($num,$decimals,$decimal_separator,$thousands_separator) . ' ' . Str::ucfirst($currency) : '';
        });
    }
}
