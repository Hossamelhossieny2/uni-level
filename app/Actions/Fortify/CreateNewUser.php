<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'username'  => ['required', 'string', 'max:255', 'unique:users'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            //'gender'    => ['required'],
            'password'  => $this->passwordRules(),
            'terms'     => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ])->validate();

        $useradd = User::create([
            'username' => $input['username'],
            'email' => $input['email'],
            'gender' => 'male',
            'password' => Hash::make($input['password']),
        ]);

        $userBalance = new UserBalance;
        $userBalance->user_id = $useradd->id;
        $userBalance->save();

        return $useradd;
    }
}
