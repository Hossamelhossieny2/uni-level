<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInvest extends Model
{
    use HasFactory;

    public function plans()
    {
        return $this->belongsTo(InvestPlan::class,'invest_id');
    }

    
}
