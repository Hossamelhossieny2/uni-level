<x-app-layout>

	@push('breadcrumb')
    <h1>
        Finance
        <small>My Wallet</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">My Wallet</a></li>
        <li class="breadcrumb-item active">add Wallet</li>
      </ol>
      @endpush

      @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
        @foreach ($errors->all() as $error)
                <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
            @endforeach

<div class="box bg-yellow bg-hexagons-dark">
    <div class="box-body">
      <h3 class="mt-0">add USTD Wallet</h3>
    </div>
</div>

<div class="row">
		<div class="col-lg-12 col-12">
		  <div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">My USDT Wallet's</h3>
			  <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-center">
                add new USDT address
              </button>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover no-margin">
					  <tbody>
					  	@foreach($wallets as $wallet)
						<tr>
						  <td>Address</td>
						  <td>
						  	<span id="copied-address" style="display: none;color: #FBAD1C;">Copied!</span>
						  	
						  	<input type="text" class="form-control" id="address-to-copy" placeholder="Enter text" value="{{$wallet->wallet}}" />
							    <button id="copy-address" class="form-control">Copy</button>
							    
						  </td>
						
						  <td>Wallet name</td>
						  <td align="center"><a href="#">{{$wallet->wallet_name}}</a><br>{{$wallet->wallet_email}}</td>
						
							<td colspan="2">
								{!! QrCode::size(150)->generate('usdt:{{$wallet->wallet}}'); !!}
							</td>
							<td>
								<a href="{{ route('delete_address',[app()->getLocale(),$wallet->id]) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');" >
								    Delete This !
								</a>
							</td>
						</tr>
						@endforeach
					  </tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		  </div>
		</div>
		
	  </div>

			  <div class="modal center-modal fade" id="modal-center" tabindex="-1">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title">Add New USDT Address</h5>
						<form method="post" action="{{ route('add_address',app()->getLocale()) }}">
							@csrf
						<button type="button" class="close" data-dismiss="modal">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<input type="text" class="form-control" name="address" placeholder="address here">
						<input type="email" class="form-control" name="wallet_email" placeholder="wallet email here">
						<input type="text" class="form-control" name="wallet_name" placeholder="wallet name here">
					  </div>
					  <div class="modal-footer modal-footer-uniform">
						<button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-bold btn-pure btn-primary float-right">Save new</button>
					  </div>
					  </form>
					</div>
				  </div>
				</div>

	  @push('scripts')
	  	<script type="text/javascript">
	  		// Add click event
			document.getElementById('copy-address').addEventListener('click', function(e){
			  e.preventDefault();
			  
			  // Select the text
			  document.getElementById('address-to-copy').select();
			  
			  var copied;
			  
			  try
			  {
			      // Copy the text
			      copied = document.execCommand('copy');
			  } 
			  catch (ex)
			  {
			      copied = false;  
			  }
			  
			  if(copied)
			  {
			    // Display the copied text message
			    document.getElementById('copied-address').style.display = 'block';    
			  }
			  
			});


	  	</script>
	  	@endpush
</x-app-layout>