<li>
    <a href="javascript:void(0);">
        <div class="member-view-box">
            <div class="member-image">
                @if($user->profile_photo_path)
                    <img src="{{ asset($user->profile_photo_path)}}" alt="Member" >
                @else
                    <img src="{{ asset('img/default/user_'.$user->gender.$user->pkg.'.png')}}" alt="Member" >
                @endif
                <img src="{{ asset('img/default/flag/'.strtolower($user['country']['iso']).'.svg') }}" style="width: 15px;height: 10px;text-align: center;" />
                <div class="member-details">
                    <h6  style="color:blue">{{$user->username}}</h6>
                    
                    <span class="badge badge-pill" style="font-size:150%;color: #000">{{$user->l}}</span>
                    <span class="badge badge-pill" style="font-size:150%;color: #000">{{$user->r}}</span>
                </div>
                
                <div class="member-details">
                    <span class="badge badge-pill" style="font-size:100%;color: blue">{{$user->bvl}}</span>
                    <span class="badge badge-pill" style="font-size:100%;color: blue">{{$user->bvr}}</span>
                </div>
            </div>
        </div>
    </a>
    @if(isset($tree[$user->id]))
    <ul>
    @foreach($tree[$user->id] as $key=>$member)
        @if(count($tree[$user->id])>1)
            <x-child :user="$member" :tree="$tree"></x-child>
        @else
            

            @if(!empty($key) && $key == 'l')
                <x-child :user="$member" :tree="$tree"></x-child>
            @else
                <x-empty ></x-empty>
            @endif

            @if(!empty($key) && $key == 'r')
                <x-child :user="$member" :tree="$tree"></x-child>
            @else
                <x-empty ></x-empty>
            @endif

        @endif
    @endforeach
    </ul>
    @endif
</li>