<tr>
    <td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
    <td class="w-20">
        <a class="avatar" href="#">
        <img src="{{ asset('img/default/flag/'.strtolower($user['country']['iso']).'.svg') }}"  />
    </a>
    </td>
    <td class="w-60">
      <a class="avatar avatar-lg status-success" href="#">
        @if($user->profile_photo_path)
            <img src="{{ asset($user->profile_photo_path)}}" alt="{{$user->username}}" >
        @else
            <img src="{{ asset('img/default/user_'.$user->gender.$user->pkg.'.png')}}" alt="{{$user->username}}">
        @endif
      </a>
    </td>
    <td class="w-300">
        <p class="mb-0">
          <a href="#"><strong>{{$user->username}}</strong></a>
          <small class="sidetitle">{{$user->email}}</small>
        </p>
        <p class="mb-0 text-yellow">{{ 'US1010'.$user->id}}</p>
    </td>
    <td>
        <nav class="nav mt-2">
          <span class="badge badge-pill" style="color: #000;font-size:150%;">{{$user->l}}</span>
            <span class="badge badge-pill" style="color: #000;font-size:100%;">{{$user->bvl}}</span>
            <span class="badge badge-pill" style="color: #000;font-size:100%;">{{$user->bvr}}</span>
            <span class="badge badge-pill" style="color: #000;font-size:150%;">{{$user->r}}</span>
        </nav>
    </td>
</tr>
@if(isset($tree[$user->id]))
  @foreach($tree[$user->id] as $key=>$member)
    <x-child_tr :user="$member" :tree="$tree"></x-child_tr>
  @endforeach
@endif