<x-app-layout>

@push('breadcrumb')
    <h1>
        Finance
        <small>deposit Requests</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">My Wallet</a></li>
        <li class="breadcrumb-item active">charge request</li>
      </ol>
      @endpush

 <div class="box box-inverse box-dark bg-hexagons-white">
        <div class="box-body">
			<div class="row">
				<div class="col-md-3 col-12">
				 	<div class="text-center">
                        <h1 class="text-bold count">325</h1>
                        <hr class="w-50 my-5 b-3 border-yellow">
                        <h5 class="mt-10">Transactions in last 24h</h5>
                    </div>
				</div>
				<div class="col-md-3 col-12">
				 	<div class="text-center">
                        <h1 class="text-bold count">40</h1>
                        <hr class="w-50 my-5 b-3 border-yellow">
                        <h5 class="mt-10">Transactions per hour</h5>
                    </div>
				</div>
				<div class="col-md-3 col-12">
				 	<div class="text-center">
                        <h1 class="text-bold count">150</h1>
                        <hr class="w-50 my-5 b-3 border-yellow">
                        <h5 class="mt-10">Largest Transactions</h5>
                    </div>
				</div>
				<div class="col-md-3 col-12">
				 	<div class="text-center">
                        <h1 class="text-bold count">8</h1>
                        <hr class="w-50 my-5 b-3 border-yellow">
                        <h5 class="mt-10">Years of Experience</h5>
                    </div>
				</div>
			</div>
        </div>
        <!-- /.box-body -->
      </div>      
      <!-- /.box -->
   @foreach ($errors->all() as $error)
                <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
            @endforeach
            @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
        @endif
   <div class="box-body wizard-content">
   	<form method="post" class="" action="{{ route('add_charge_request',app()->getLocale()) }}">
   		@csrf
					<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<select class="form-control" name="wallet">
								  		@foreach($wallets as $wallet)
								  		<option value="{{$wallet->wallet}}">{{$wallet->wallet_name .' :  . . . . . . '.substr($wallet->wallet,12)}}</option>
								  		@endforeach
								  	</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<input type="email" name="email" class="form-control" placeholder="Email Address" value="{{ auth()->user()->email }}"> 
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<input type="numeric" name="amount" class="form-control" placeholder="amount requested"> 
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<input type="password" name="trans_pass" class="form-control" placeholder="Transactions password"> 
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									@if(!empty($wallet->wallet_name))
			                      <button type="submit" class="btn btn-info btn-flat form-control">Go!</button>
			                      @else
			                      <a href="{{ route('my_wallet',app()->getLocale()) }}" class="btn btn-info btn-danger form-control">press to add wallet first !</a>
			                      @endif
		                        </div>
	                        </div>
				        
					</div>
					</form>
					
				</div>

   


	  <div class="box bg-yellow bg-hexagons-dark">
        <div class="box-body">
			<h3 class="mt-0">My Charge Requests</h3>
        </div>
        <!-- /.box-body -->
      </div>      
      <!-- /.box -->
      
      <div class="row">
      	<div class="col-12">
      		@foreach($requests as $req)
      		<div class="box bg-hexagons-dark">
		        <div class="box-body">
					<div>
						<div class="row justify-content-between">
						  <div class="col-md-auto">
							  <h5><a href="#">From Wallet Email : {{$req->email}}</a></h5>
						  </div>
						  <div class="col-md-auto">
							  <h5 class="text-right">requested at : {{$req->created_at}}</h5>
						  </div>
						</div>	

						<div class="row justify-content-between">
						  <div class="col-md-auto">
							  <h5><a href="#">From Selected wallet : {{$req->wallet}}</a>
							  <i class="fa fa-long-arrow-right text-green px-15"></i>
							  <a href="#">company USDT address</a></h5>
						  </div>
						  <div class="col-md-auto">
							  <h5 class="text-bold text-right">TOTAL amount ( {{$req->amount}} ) USDT</h5>
						  </div>
						</div>
						@if($req->accepted == 0)
						<div class="text-right mt-30">
							<a href="{{ route('cancel_request',[app()->getLocale(),$req->id]) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this request?');">cancel this request</a>
							<span class="badge badge-xl badge-warning">pending</span>
						</div>
						@else
						<div class="text-right mt-30">
							<span class="badge badge-xl badge-success">done on: {{ $req->charge_date }}</span>
						</div>
						@endif
					</div>
		        </div>
		        <!-- /.box-body -->
		      </div>
		      @endforeach
      	   </div>
      </div>
	     
      
	  



      @push('scripts')
      	<!-- Crypto_Admin for demo purposes -->
		<script src="{{ asset('js/demo.js') }}"></script>
		<script>
	        $('.count').each(function () {
				$(this).prop('Counter',0).animate({
					Counter: $(this).text()
				}, {
					duration: 4000,
					easing: 'swing',
					step: function (now) {
						$(this).text(Math.ceil(now));
					}
				});
			}); 
	      </script>
      @endpush

</x-app-layout>