<x-app-layout>

@push('breadcrumb')
    <h1>
        Transactions
        <small>User Transfer</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Transactions</a></li>
        <li class="breadcrumb-item active">User Transfer</li>
      </ol>
      @endpush

 @foreach ($errors->all() as $error)
                <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
            @endforeach

            @if(Session::has('success'))
	        <div class="alert alert-success">
	            {{ Session::get('success') }}
	        </div>
	        @endif

            @if(Session::has('error'))
	        <div class="alert alert-danger">
	            {{ Session::get('error') }}
	        </div>
	        @endif

<div class="row">

						<?php $in = $out = 0;?>
						@foreach($trans as $tran)
								@if($tran->trans == 'in')
									<?php $in += $tran->amount;?>
								@endif
								@if($tran->trans == 'out' && auth()->id() != 1)
									<?php $out += $tran->amount;?>
								@endif
							
						@endforeach
						
		<div class="col-8">

          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Transfer To another USER</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="{{ route('do_user_trans',app()->getLocale()) }}" role="form">
              	@csrf
                <!-- text input -->
                <div class="form-group col-8">
                  <label>Transfer amount</label>
                  <input type="number" class="form-control" placeholder="Enter amount to Transfer..." name="amount">
                </div>
                <div class="form-group col-8">
                  <label>User Email</label>
                  <input type="text" class="form-control" placeholder="Enter user email..." name="user_email">
                </div>
                <div class="form-group col-8">
                  <label>Your Trans pass</label>
                  <input type="password" class="form-control" placeholder="Enter your trans pass..." name="trans_pass">
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Transfer</button>
            </div>
             </form>
            </div>

          </div>

          <div class="col-4">
          	<div class="box">
              <div class="box-body">
                <div class="flexbox">
                  <h5>Account in summary</h5>
                  <div class="dropdown">
                    <span class="dropdown-toggle no-caret" data-toggle="dropdown"><i class="ion-android-more-vertical rotate-90"></i></span>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#"><i class="ion-android-list"></i> Details</a>
                      <a class="dropdown-item" href="#"><i class="ion-android-add"></i> Add new</a>
                      <a class="dropdown-item" href="#"><i class="ion-android-refresh"></i> Refresh</a>
                    </div>
                  </div>
                </div>

                <div class="text-center my-2">
                  <div class="font-size-60 text-info">${{$in}}</div>
                  <span class="text-muted">Deposit</span>
                </div>
              </div>

              <div class="box-body bg-gray-light py-12">
                <span class="text-muted mr-1">all Transactions:</span>
                <span class="text-dark">{{ count($trans)}}</span>
              </div>

              <div class="progress progress-xxs mt-0 mb-0">
                <div class="progress-bar bg-success" role="progressbar" style="width: 65%; height: 3px;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
            <div class="box">
              <div class="box-body">
                <div class="flexbox">
                  <h5>Account out summary</h5>
                  <div class="dropdown">
                    <span class="dropdown-toggle no-caret" data-toggle="dropdown"><i class="ion-android-more-vertical rotate-90"></i></span>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#"><i class="ion-android-list"></i> Details</a>
                      <a class="dropdown-item" href="#"><i class="ion-android-add"></i> Add new</a>
                      <a class="dropdown-item" href="#"><i class="ion-android-refresh"></i> Refresh</a>
                    </div>
                  </div>
                </div>

                <div class="text-center my-2">
                  <div class="font-size-60 text-pink">${{$out}}</div>
                  <span class="text-muted">Withdrawd</span>
                </div>
              </div>

              <div class="box-body bg-gray-light py-12">
                <span class="text-muted mr-1">all Transactions:</span>
                <span class="text-dark">{{ count($trans)}}</span>
              </div>

              <div class="progress progress-xxs mt-0 mb-0">
                <div class="progress-bar bg-success" role="progressbar" style="width: 65%; height: 3px;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
        </div>
          

        </div>

       
        <!-- /.col -->
      </div>

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>