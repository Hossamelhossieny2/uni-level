<x-app-layout>

@push('breadcrumb')
      <h1>
        My Invest revenu
        <small>invest income</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
        <li class="breadcrumb-item active">My Invest revenu</li>
      </ol>
@endpush

<div class="row">
        <div class="col-12">
         
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Invest revenu Income</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
					<thead>
						<tr class="bg-info font-weight-100 font-size-16">
							<th class="text-white">Trans id</th>
							<th class="text-white">Plan desc</th>
							<th class="text-white">Amount</th>
							<th class="text-white">Time</th>
							<th class="text-white">action</th>
						</tr>
					</thead>
					<tbody>
						<!-- @foreach($trans as $tran)
						<tr>
							<td>{{$tran->id}}</td>
							<td>{{$tran->desc}}</td>
							<td align="center">
									<span class="badge badge-pill ">0</span>
							</td>
							<td>{{$tran->created_at}}</td>
							<td> -->
								<!-- <span class="badge badge-xl badge-purple">complain</span> -->
							<!-- </td>
						</tr> 
						@endforeach -->
						<tr>
							<td></td>
							<td></td>
							<td align="center">
									<span class="badge badge-pill ">0</span>
							</td>
							<td></td>
							<td></td>
						</tr>

					</tbody>
					<tfoot>
						<tr class="bg-info">
							<th></th>
							<th></th>
							<th></th>
							<th class="text-white font-weight-100 font-size-20">0</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
			
        </div>
        <!-- /.col -->
      </div>

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>