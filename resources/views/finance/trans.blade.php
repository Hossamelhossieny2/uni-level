<x-app-layout>

@push('breadcrumb')
      <h1>
        Transactions
        <small>All Trans</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
        <li class="breadcrumb-item active">all Transactions</li>
      </ol>
@endpush

<div class="row">
        <div class="col-12">
         
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">all User Transactions</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
					<thead>
						<tr class="bg-info font-weight-100 font-size-16">
							<th class="text-white">Transaction</th>
							<th class="text-white">Title</th>
							<th class="text-white">Type</th>
							<th class="text-white">in</th>
							<th class="text-white">out</th>
							<th class="text-white">Time</th>
							<th class="text-white">action</th>
						</tr>
					</thead>
					<tbody>
						<?php $in = $out = 0;?>
						@foreach($trans as $tran)
						<tr>
							<td>{{$tran->id}}</td>
							<td>{{$tran->desc}}</td>
							<td align="center" class="font-weight-100 font-size-30">
								@if($tran->trans=='in')
									+
								@else
									-
								@endif
							</td>
							<td align="center">
								@if($tran->trans == 'in')
									{{$tran->amount}}
									<?php $in += $tran->amount;?>
								@else
									0
								@endif
							</td>
							<td align="center">
								@if($tran->trans == 'out' && auth()->id() != 1)
									{{$tran->amount}}
									<?php $out += $tran->amount;?>
								@else
									0
								@endif
							</td>
							<td>{{$tran->created_at}}</td>
							<td>
								<!-- <span class="badge badge-xl badge-purple">complain</span> -->
							</td>
						</tr>
						@endforeach
						
					</tbody>
					<tfoot>
						<tr class="bg-gray">
							<th></th>
							<th></th>
							<th></th>
							<th class="font-weight-100 font-size-20">{{$in}}</th>
							<th class="font-weight-100 font-size-20">{{$out}}</th>
							<th class="">your balance</th>
							<th class="font-weight-200 font-size-30" style="color:#000">{{$user['balance']['balance']}}</th>
						</tr>
					</tfoot>
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
			
        </div>
        <!-- /.col -->
      </div>

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>