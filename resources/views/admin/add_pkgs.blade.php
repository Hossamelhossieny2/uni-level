<x-app-layout>

@push('breadcrumb')
    <h1>
        Admin
        <small>control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">charge site</li>
      </ol>
      @endpush


        @if(Session::has('success'))
	        <div class="alert alert-success">
	            {{ Session::get('success') }}
	        </div>
        @endif

        @if(Session::has('error'))
	        <div class="alert alert-danger">
	            {{ Session::get('error') }}
	        </div>
        @endif
<section class="content">
  <div class="row">
    @foreach($requests as $req)
    <div class="col-6 col-md-4 col-xl-2">
			<a class="box box-link-shadow text-center pull-up" href="{{ route('change_pkgs',[app()->getLocale(),$req->id]) }}">
				<div class="box-body py-25 bg-dark">
					<p class="font-weight-600">{{ $req->name }}</p>
				</div>
				<div class="box-body">
					<p class="mt-5">
						<i class="icon-bag fa-4x text-danger"></i>
            <br>{{ $req->price }}
					</p>
				</div>
			</a>
		</div>
    @endforeach
  </div>
</section>

@if(!empty($pkg))
<form role="form" class="form-element" method="post">
<input type="hidden" name="id" value="{{$pkg['id']}}" />
@csrf
<div class="row">

  <div class="col-6">
    
    <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title text-center">change pkgs name</h3>
        </div>
      
        <div class="box-body">
          
          <div class="form-group">
            <label for="exampleInputEmail1">package name</label>
            <input type="text" class="form-control" name="name" value="{{$pkg['name']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package price</label>
            <input type="text" class="form-control" name="price" value="{{$pkg['price']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package Maxout</label>
            <input type="text" class="form-control" name="max_out" value="{{$pkg['max_out']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package invest</label>
            <input type="text" class="form-control" name="invest" value="{{$pkg['invest']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package invest count</label>
            <input type="text" class="form-control" name="invest_count" value="{{$pkg['invest_count']}}" required>
          </div>
        </div>
        <div class="box-footer">
          PLS review second row ..!
        </div>
    </div>
  </div>

  <div class="col-6">
    <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title text-center"> ( {{$pkg['name']}} )</h3>
        </div>
      
        <div class="box-body">
          
          <div class="form-group">
            <label for="exampleInputEmail1">package BVP</label>
            <input type="text" class="form-control" name="bvp" value="{{$pkg['bvp']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package direct</label>
            <input type="text" class="form-control" name="direct" value="{{$pkg['direct']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package pair</label>
            <input type="text" class="form-control" name="pair" value="{{$pkg['pair']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">package color</label>
            <input type="text" class="form-control" name="color" value="{{$pkg['color']}}" required>
          </div>

          <div class="form-group">
            <label>package activation</label>
            <select class="form-control" name="active" required>
              <option value="1">active</option>
              <option value="0">un active</option>
            </select>
          </div>

        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      
    </div>

  </div>

</div>
</form>

@else

          <div class="alert alert-success">
	            choose any package to edit
	        </div>


@endif
            
          

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>