<x-app-layout>

@push('breadcrumb')
    <h1>
        Admin
        <small>control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">charge site</li>
      </ol>
      @endpush


        @if(Session::has('success'))
	        <div class="alert alert-success">
	            {{ Session::get('success') }}
	        </div>
        @endif

        @if(Session::has('error'))
	        <div class="alert alert-danger">
	            {{ Session::get('error') }}
	        </div>
        @endif

<div class="row">
  <div class="col-12">
    
    <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title text-center">change site words</h3>
        </div>
      <form role="form" class="form-element" method="post">
      @csrf
        <div class="box-body">
          <div class="form-group">
            <label>Select</label>
            <select class="form-control" name="word" required>
              <option value="">select word to change ... </option>
              @foreach($requests as $req)
              <option value="{{ $req->word }}">{{ $req->value }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">new word to replace</label>
            <input type="text" class="form-control" name="replace" placeholder="new word to replace here ..." required>
          </div>

        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>

  </div>

  <div class="col-12">
    
    <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title text-center">all site words</h3>
        </div>
      
        <div class="box-body">
          <div class="form-group">
            <ul>
              @foreach($requests as $req)
              <li>{{$req['value']}}</li>
              @endforeach
            </ul>
            
          </div>
          

        </div>
      
    </div>

  </div>
</div>
            
          

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>