<x-app-layout>

@push('breadcrumb')
    <h1>
        Admin
        <small>control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">charge site</li>
      </ol>
      @endpush


        @if(Session::has('success'))
	        <div class="alert alert-success">
	            {{ Session::get('success') }}
	        </div>
        @endif

        @if(Session::has('error'))
	        <div class="alert alert-danger">
	            {{ Session::get('error') }}
	        </div>
        @endif
<section class="content">
  <div class="row">
    @foreach($requests as $req)
    <div class="col-6 col-md-4 col-xl-2">
			<a class="box box-link-shadow text-center pull-up" href="{{ route('change_invest_plans',[app()->getLocale(),$req->id]) }}">
				<div class="box-body py-25 bg-dark">
					<p class="font-weight-600">{{ $req['title'] }}</p>
				</div>
				<div class="box-body">
					<p class="mt-5">
          <?php $flag = \App\Models\Country::find($req['country'])->iso;?>
						<img src="{{ asset('img/default/flag/'.strtolower($flag).'.svg') }}" width="100" height="50" />
					</p>
				</div>
			</a>
		</div>
    @endforeach
  </div>
</section>

@if(!empty($plan))
<form role="form" class="form-element" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="{{$plan['id']}}" />
@csrf
<div class="row">
  <div class="col-12">
    <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title text-center"> ( {{$plan['title']}} )</h3><br>
          <img src="{{asset($plan['image'])}}" />
        </div>
      
        <div class="box-body">
          
          <div class="form-group">
            <label for="exampleInputEmail1">Plan image</label>
            <input class="form-control mb-2" type="file"
           accept="image/png"
           name="image" >
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan Title</label>
            <input type="text" class="form-control" name="title" value="{{$plan['title']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan small tilte</label>
            <input type="text" class="form-control" name="title_small" value="{{$plan['title_small']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan Currency</label>
            <input type="text" class="form-control" name="currency" value="{{$plan['currency']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan Sign</label>
            <input type="text" class="form-control" name="sign" value="{{$plan['sign']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan description</label>
            <input type="text" class="form-control" name="desc" value="{{$plan['desc']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan volume</label>
            <input type="text" class="form-control" name="volume" value="{{$plan['volume']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan members</label>
            <input type="text" class="form-control" name="members" value="{{$plan['members']}}" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Plan invest cap</label>
            <input type="text" class="form-control" name="invest_cap" value="{{$plan['invest_cap']}}" required>
          </div>

           <div class="form-group">
            <label for="exampleInputEmail1">Plan start from</label>
            <input type="date" class="form-control" name="start" value="{{$plan['start']}}" required>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Plan start end</label>
            <input type="date" class="form-control" name="end" value="{{$plan['end']}}" required>
          </div>

          <div class="form-group">
            <label>invest country</label>
            <select class="form-control" name="country" required>
              <option value="">select country</option>
              @foreach($country as $c)
                <option value="{{$c['id']}}" @if($c['id'] == $plan['country']) selected @endif>{{$c['nicename']}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Plan activation</label>
            <select class="form-control" name="status" required>
              <option value="active">active</option>
              <option value="notactive">un active</option>
            </select>
          </div>

        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      
    </div>

  </div>

</div>
</form>

@else

          <div class="alert alert-success">
	            choose any package to edit
	        </div>


@endif
            
          

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>