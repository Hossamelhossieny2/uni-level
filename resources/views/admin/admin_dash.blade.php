<x-app-layout>

	@push('breadcrumb')
    <h1>
        Admin
        <small>control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">account</li>
      </ol>
      @endpush


<div class="row">
    <div class="col-xl-3 col-md-6 col-12">
            <div class="box box-body box-inverse bg-info">
              <h6>
                <span class="text-uppercase">BALANCE</span>
                <span class="float-right"><a class="btn btn-xs btn-warning" href="{{ route('accept_charge',app()->getLocale()) }}">View</a></span>
              </h6>
              <br>
              <p class="font-size-26">${{ number_format($admin->balance) }}</p>
              <?php $rest = intval(($admin->balance *100)/100000)?>
              <div class="progress progress-xxs mt-0 mb-10">
                <div class="progress-bar bg-warning" role="progressbar" style="width: {{$rest}}%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <div class="font-size-12"><i class="ion-arrow-graph-down-right text-warning mr-1"></i> %{{100-$rest}} down</div>
            </div>
        </div>

         <div class="col-xl-3 col-md-6 col-12">
            <div class="box box-body box-inverse bg-pink">
              <h6>
                <span class="text-uppercase">Charged</span>
                <span class="float-right"><a class="btn btn-xs btn-warning" href="{{ route('accept_charge',app()->getLocale()) }}">View</a></span>
              </h6>
              <br>
              <p class="font-size-26">${{ number_format($charge) }}</p>
              <div class="progress progress-xxs mt-0 mb-10">
              </div>
              <div class="font-size-12"><i class="ion-arrow-graph-down-right text-warning mr-1"></i> {{ $charge_number}} Member</div>
            </div>
        </div>
       
    </div>


</x-app-layout>
