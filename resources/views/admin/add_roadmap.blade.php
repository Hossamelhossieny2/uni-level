<x-app-layout>

@push('breadcrumb')
    <h1>
        Admin
        <small>control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Roadmap</li>
      </ol>
      @endpush


        @if(Session::has('success'))
	        <div class="alert alert-success">
	            {{ Session::get('success') }}
	        </div>
        @endif

        @if(Session::has('error'))
	        <div class="alert alert-danger">
	            {{ Session::get('error') }}
	        </div>
        @endif

<div class="row">
        <div class="col-12">
         
          <div class="box box-solid">
            <div class="box-header with-border">
              <div>
					<h3 class="box-title text-center">add site Roadmap</h3>
			</div>
            </div>
            
            <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title text-center">change site road map</h3>
        </div>
      <form role="form" class="form-element" method="post">
      @csrf
        <div class="box-body">
          
          <div class="form-group">
            <label for="exampleInputEmail1">Event time</label>
            <input type="text" class="form-control" name="date" placeholder="jan 2021 q1" required>
          </div>

		  <div class="form-group">
            <label for="exampleInputEmail1">Event title</label>
            <input type="text" class="form-control" name="word" placeholder="event title" required>
          </div>

		  <div class="form-group">
            <label for="exampleInputEmail1">Event description</label>
            <input type="text" class="form-control" name="val" placeholder="event description" required>
          </div>

		  <div class="form-group">
            <label for="exampleInputEmail1">Event sorting</label>
            <input type="text" class="form-control" name="sort" placeholder="type sort" required>
          </div>


        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>

            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
					<thead>
						<tr>
							<th>date</th>
							<th>title</th>
							<th>description</th>
							<th>sort</th>
							<th>action</th>
						</tr>
					</thead>
					<tbody>

						@foreach($requests as $req)
						<tr>
							<td>{{$req['dat']}}</td>
							<td>{{$req['tit']}}</td>
							<td>{{$req['des']}}</td>
							<td>{{$req['sort']}}</td>
							<th><a href="{{ route('del_roadmap',[app()->getLocale(),$req['id']]) }}" class="btn btn-danger">delete</a></th>
						</tr>
						@endforeach
					</tbody>				  

				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
			
        </div>
        <!-- /.col -->
      </div>

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>