<x-app-layout>

@push('breadcrumb')
    <h1>
        Admin
        <small>control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">charge accept</li>
      </ol>
      @endpush


        @if(Session::has('success'))
	        <div class="alert alert-success">
	            {{ Session::get('success') }}
	        </div>
        @endif

        @if(Session::has('error'))
	        <div class="alert alert-danger">
	            {{ Session::get('error') }}
	        </div>
        @endif

<div class="row">
        <div class="col-12">
         
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              
              
              <div>
				<a class="box text-right" href="javascript:void(0)">
					<h3 class="box-title text-center">User Charge Requests</h3>
					<div class="box-body">
						<p class="font-size-40" style="color:grey;">
							<strong><i class="fa fa-money text-muted mr-5"></i> {{ $admin->balance }}</strong>
						</p>
					</div>
				</a>
			</div>
            </div>
            
            

            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
					<thead>
						<tr>
							<th>Name</th>
							<th>Wallet / Address</th>
							<th>Wallet email</th>
							<th>package</th>
							<th>amount</th>
							<th>request date</th>
							<th>action</th>
						</tr>
					</thead>
					<tbody>

						@foreach($requests as $req)
						
						<tr>
							<td>{{$req['user']['username']}}</td>
							<td>{{$req->wallet}}</td>
							<td>{{$req->email}}</td>
							<td align="center">
								<span class="badge badge-pill badge-purple">{{$req['user']['pkg']}}</span>
							</td>
							<td align="center">
								<span class="badge badge-xl badge-info">{{$req->amount}}</span>
							</td>
							<td>{{$req->created_at}}</td>
							<td>
								@if($req->accepted == 1)
									<span class="badge badge-xl badge-purple">{{$req->charge_date}}</span>
								@else
									@if($admin->charge == 1)
									<form method="post" action="{{ route('do_charge',app()->getLocale()) }}">
										@csrf
										<input type="hidden" name="otp" value="{{ serialize(auth()->user()->password) }}">
										<input type="hidden" name="requested" value="{{ $req->id }}">
										<button type="submit" class="btn btn-success" >Charge</button>
									</form>
									@else
									<span class="badge badge-xl badge-danger">no permisions</span>
									@endif
								@endif
							</td>
						</tr>
					
						@endforeach
						
					</tbody>				  
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Wallet / Address</th>
							<th>Wallet email</th>
							<th>package</th>
							<th>amount</th>
							<th>request date</th>
							<th>action</th>
						</tr>
					</tfoot>
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
			
        </div>
        <!-- /.col -->
      </div>

@push('scripts')
<!-- This is data table -->
    <script src="../../../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.j') }}s"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- Crypto_Admin for Data Table -->
	<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush
</x-app-layout>