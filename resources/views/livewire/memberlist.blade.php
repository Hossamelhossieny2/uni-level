<div>
     
     @push('breadcrumb')
    <h1>
        Members
        <small>genealogy List</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Members List</li>
      </ol>
      @endpush
        
        <div class="row">
          <div class="col-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                      <table id="example5" class="table table-hover">
                        <thead class="d-none">
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
                                <td class="w-20">
                                    <a class="avatar" href="#">
                                    <img src="{{ asset('img/default/flag/'.strtolower($mainUser['country']['iso']).'.svg') }}"  />
                                </a>
                                </td>
                                <td class="w-60">
                                  <a class="avatar avatar-lg status-success" href="#">
                                    @if($mainUser->profile_photo_path)
                                        <img src="{{ asset($mainUser->profile_photo_path)}}" alt="Member" >
                                    @else
                                        <img src="{{ asset('img/default/user_'.$mainUser->gender.$mainUser->pkg.'.png')}}" alt="Member" >
                                    @endif
                                  </a>
                                </td>
                                <td class="w-300">
                                    <p class="mb-0">
                                      <a href="#"><strong>{{$mainUser->username}}</strong></a>
                                      <small class="sidetitle">{{$mainUser->email}}</small>
                                    </p>
                                    <p class="mb-0 text-yellow">{{ 'US1010'.$mainUser->id}}</p>
                                </td>
                                <td>
                                    <nav class="nav mt-2">
                                        <span class="badge badge-pill" style="color: #000;font-size:150%;">{{$mainUser->l}}</span>
                                        <span class="badge badge-pill" style="color: #000;font-size:100%;">{{$mainUser->bvl}}</span>
                                        <span class="badge badge-pill" style="color: #000;font-size:100%;">{{$mainUser->bvr}}</span>
                                        <span class="badge badge-pill" style="color: #000;font-size:150%;">{{$mainUser->r}}</span>
                                    </nav>
                                </td>
                            </tr>
                            @if(isset($tree[$mainUser->id]))    
                                @foreach($tree[$mainUser->id] as $key=>$member)
                                    <x-person_tr :user="$member" :tree="$tree"></x-person_tr>
                                @endforeach
                            @endif
                        </tbody>
                      </table>
                    </div>
                </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
          </div>            
        </div>

        @push('scripts')
        <!-- This is data table -->
        <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    @endpush
</div>
