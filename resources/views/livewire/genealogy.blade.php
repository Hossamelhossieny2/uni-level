<div>
    

   @push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tree.css') }}">
    @endpush
    
   @push('breadcrumb')
    <h1>
        Members
        <small>genealogy Tree</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">genealogy</li>
      </ol>
      @endpush

      <div class="row">
            
            <div class="col-xl-3 col-md-6 col-12 ">
                <div class="box box-body pull-up bg-primary bg-deathstar-white">
                  <div class="flexbox">
                    <span class="ion ion-ios-person-outline font-size-50"></span>
                    <span class="font-size-40 font-weight-200">{{$mainUser->l}}</span>
                  </div>
                  <div class="text-right">Users Left</div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
              <div class="box box-body pull-up text-center bg-info bg-deathstar-white">
                  <div class="font-size-40 font-weight-200">+{{$mainUser->bvl}}</div>
                  <div>Zoms Left</div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
              <div class="box box-body pull-up text-center bg-info bg-deathstar-white">
                  <div class="font-size-40 font-weight-200">+{{$mainUser->bvr}}</div>
                  <div>Zoms Right</div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12 ">
                <div class="box box-body pull-up bg-primary bg-deathstar-white">
                  <div class="flexbox">
                    <span class="font-size-40 font-weight-200">{{$mainUser->r}}</span>
                    <span class="ion ion-ios-person-outline font-size-50"></span>
                  </div>
                  <div>Users Right</div>
                </div>
            </div>

        </div>

    <div class="genealogy-body genealogy-scroll box-body bg-deathstar-white">
        <div class="genealogy-tree">
            <ul>
                <li>
                    <a href="javascript:void(0);">
                        <div class="member-view-box">
                            <div class="member-image text-center">
                                @if($mainUser->profile_photo_path)
                                    <img src="{{ asset($mainUser->profile_photo_path)}}" alt="Member" style="border:2px dashed {{$mainUser['pkgs']['color']}};">
                                @else
                                    <img src="{{ asset('img/default/user_'.$mainUser->gender.$mainUser->pkg.'.png')}}" alt="Member">
                                @endif
                                <img src="{{ asset('img/default/flag/'.strtolower($mainUser['country']['iso']).'.svg') }}" style="width: 15px;height: 10px;text-align: center;" />
                                <div class="member-details">
                                    <h6 style="color:blue">{{$mainUser->username}}</h6>
                                    
                                    <span class="badge badge-pill" style="font-size:150%;color: #000">{{$mainUser->l}}</span>
                                    <span class="badge badge-pill" style="font-size:150%;color: #000">{{$mainUser->r}}</span>
                                </div>
                                <div class="member-details">
                                    <span class="badge badge-pill" style="font-size:100%;color: blue">{{$mainUser->bvl}}</span>
                                    <span class="badge badge-pill" style="font-size:100%;color: blue">{{$mainUser->bvr}}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <ul  class="active">
                    @if(isset($tree[$mainUser->id]))    
                    @foreach($tree[$mainUser->id] as $key=>$member)
                    @if(count($tree[$mainUser->id])>1)
                        <x-person :user="$member" :tree="$tree"></x-person>
                    @else
                        

                        @if(!empty($key) && $key == 'l')
                            <x-person :user="$member" :tree="$tree"></x-person>
                        @else
                            <x-empty ></x-empty>
                        @endif

                        @if(!empty($key) && $key == 'r')
                            <x-person :user="$member" :tree="$tree"></x-person>
                        @else
                            <x-empty ></x-empty>
                        @endif

                    @endif
                    
                    @endforeach
                    @endif
                </ul>
                </li>
            </ul>
        </div>
    </div>

@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>
    
    
    <script type="text/javascript">
        $(document).ready(function(){

            $(function () {
                $('.genealogy-tree ul').hide();
                $('.genealogy-tree>ul').show();
                $('.genealogy-tree ul.active').show();
                $('.genealogy-tree li').on('click', function (e) {
                    var children = $(this).find('> ul');
                    if (children.is(":visible")) children.hide('fast').removeClass('active');
                    else children.show('fast').addClass('active');
                    e.stopPropagation();
                });
            });

        });
        
    </script>
@endpush

</div>
