<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <b class="logo-mini">
          <span class="light-logo"><img src="../images/logo-light.png" alt="logo"></span>
          <span class="dark-logo"><img src="../images/logo-dark.png" alt="logo"></span>
      </b>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
          <img src="../images/logo-light-text.png" alt="logo" class="light-logo">
          <img src="../images/logo-dark-text.png" alt="logo" class="dark-logo">
      </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
                  
          
          
          <!-- Notifications -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
            </a>
            <ul class="dropdown-menu scale-up">
              <li class="header">You have 0 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu inner-content-div">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> no new notifications.
                    </a>
                  </li>
                  
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          
          <!-- User Account -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               @if(Auth::user()->profile_photo_path)
                <img class="user-image rounded-circle" src="{{ asset(Auth::user()->profile_photo_path) }}" alt="{{ Auth::user()->username }}" />
                @else
                <img class="user-image rounded-circle" src="{{ asset('img/default/user.png') }}" alt="{{ Auth::user()->username }}" />
                @endif
            </a>
            <ul class="dropdown-menu scale-up">
              <!-- User image -->
              <li class="user-header">
                @if(Auth::user()->profile_photo_path)
                <img class="float-left rounded-circle" src="{{ asset(Auth::user()->profile_photo_path) }}" alt="{{ Auth::user()->username }}" />
                @else
                <img class="float-left rounded-circle" src="{{ asset('img/default/user.png') }}" alt="{{ Auth::user()->username }}" />
                @endif
                <p>
                  Hi, {{ Auth::user()->username }}
                  <small class="mb-5">{{ Auth::user()->email }}</small>
                  <a href="#" class="btn btn-danger btn-sm btn-rounded">View Profile</a>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row no-gutters">
                  <div class="col-12 text-left">
                    <a href="{{ route('profile.show',app()->getLocale()) }}"><i class="ion ion-person"></i> My Profile</a>
                  </div>
                  <div class="col-12 text-left">
                    <a href="{{ route('api-tokens.index',app()->getLocale()) }}"><i class="ion ion-email-unread"></i> My Tokens</a>
                  </div>
                  <!-- <div class="col-12 text-left">
                    <a href="#"><i class="ion ion-settings"></i> Setting</a>
                  </div> -->
                <div role="separator" class="divider col-12"></div>
                  <!-- <div class="col-12 text-left">
                    <a href="#"><i class="ti-close"></i> Lock Screen</a>
                  </div> -->
                <div role="separator" class="divider col-12"></div>
                  <div class="col-12 text-left">
                    <form method="POST" action="{{ route('logout',app()->getLocale()) }}">
                        @csrf
                    <a href="{{ route('logout',app()->getLocale()) }}" onclick="event.preventDefault();this.closest('form').submit();">
                        <i class="fa fa-power-off"></i> Logout</a>
                        </form>
                  </div>                
                </div>
                <!-- /.row -->
              </li>
            </ul>
          </li>

          <li class="search-box">
            <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
            <form class="app-search" style="display: none;">
                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
            </form>
          </li> 
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
         <div class="ulogo">
             <a href="#">
              <!-- logo for regular state and mobile devices -->
              <span><b>Green </b>Way</span>
            </a>
        </div>
          <img src="{{ asset('img/default/zooms.gif') }}" alt="{{ Auth::user()->username }}">
        <div class="info">
          <p>{{ auth()->user()->username }}</p>
          <p>{{ auth()->user()->email }}</p>
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="nav-devider"></li>
        <!-- <li class="{{ (request()->routeIs('dashboard', app()->getLocale())) ? 'active' : '' }}">
          <a href="{{ route('dashboard',app()->getLocale()) }}">
            <i class="icon-home"></i> <span>Home</span>
          </a>
        </li> -->
        <li class="treeview {{ (request()->routeIs('mydash', app()->getLocale()) || request()->routeIs('my_invests', app()->getLocale()) || request()->routeIs('my_trans', app()->getLocale()) || request()->routeIs('my_revenu', app()->getLocale())) ? 'active' : '' }}">
          <a href="">
            <i class="fa fa-dashboard"></i> 
            <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->routeIs('mydash', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('mydash',app()->getLocale()) }}">My Dashboard</a></li>
            <li class="{{ (request()->routeIs('my_invests', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('my_invests',app()->getLocale()) }}">My invests</a></li>
            <li class="{{ (request()->routeIs('my_revenu', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('my_revenu',app()->getLocale()) }}">My Invest revenu</a></li>
            <li class="{{ (request()->routeIs('my_trans', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('my_trans',app()->getLocale()) }}">My Transcations</a></li>
          </ul>
        </li>
        <li class="treeview {{ (request()->routeIs('mytree', app()->getLocale()) || request()->routeIs('member_list', app()->getLocale())) ? 'menu-open' : '' }}">
          <a href="#">
            <i class="icon-people"></i>
            <span>Members</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(auth()->user()->pkg != 0)
            <li class="{{ (request()->routeIs('mytree', app()->getLocale()) || request()->routeIs('member_list', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('mytree',app()->getLocale()) }}">Members Grid</a></li>
            <li class="{{ (request()->routeIs('member_list', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('member_list',app()->getLocale()) }}">Members List</a></li>
            @else
            <li><a href="#" class="text-primary">Members Grid</a></li>
            <li><a href="#" class="text-primary">Members List</a></li>
            @endif
          </ul>
        </li>
        
        <li class="treeview {{ (request()->routeIs('my_wallet', app()->getLocale()) || request()->routeIs('charge_request', app()->getLocale())) ? 'active' : '' }}">
          <a href="#">
            <i class="icon-wallet"></i>
            <span>My Wallet</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->routeIs('my_wallet', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('my_wallet',app()->getLocale()) }}">add Wallet</a></li>
            <li class="{{ (request()->routeIs('charge_request', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('charge_request',app()->getLocale()) }}">deposit Requests</a></li>
            @if(auth()->user()->pkg != 0)
            <li class="{{ (request()->routeIs('withdraw_request', app()->getLocale())) ? 'active' : '' }}"><a href="{{ route('withdraw_request',app()->getLocale()) }}">withdraw Requests</a></li>
             @else
            <li><a href="#" class="text-primary">withdraw Requests</a></li>
            @endif
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-money" aria-hidden="true"></i>
            <span>Invest Plans</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(auth()->user()->pkg != 0)
            <li><a href="{{ route('invest_calculator',app()->getLocale()) }}">invest calculator</a></li>
            @foreach($invest_plans as $inv)
            <li><a href="{{ route('invest_plan',[app()->getLocale(),$inv->id]) }}"><img src="{{ asset('img/default/flag/'.strtolower($inv['it_country']['iso']).'.svg')}}" width="20"> {{$inv->title}}</a></li>
            @endforeach
            @else
            <li><a href="#" class="text-primary">invest calculator</a></li>
            @foreach($invest_plans as $inv)
            <li><a href="#" class="text-primary"><img src="{{ asset('img/default/flag/'.strtolower($inv['it_country']['iso']).'.svg')}}" width="20"> {{$inv->title}}</a></li>
            @endforeach
            @endif
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt" aria-hidden="true"></i>
            <span>Transactions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(auth()->user()->pkg != 0)
            <li><a href="{{ route('user_trans',app()->getLocale()) }}">User Transfer</a></li>
            @else
            <li><a href="#" class="text-primary">User Transfer</a></li>
            @endif
          </ul>
        </li>


        @if(auth()->user()->is_admin == 1)
        <li class="treeview">
          <a href="#">
            <i class="icon-menu"></i> <span>Admin Panel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin_dash',app()->getLocale()) }}">account overview</a></li>
            <li><a href="{{ route('accept_charge',app()->getLocale()) }}">accept charge users</a></li>
            <li><a href="{{ route('accept_withdraw',app()->getLocale()) }}">accept withdraw requests</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="icon-menu"></i> <span>WebSite Edit</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('change_words',app()->getLocale()) }}">change words</a></li>
            <li><a href="{{ route('add_roadmap',app()->getLocale()) }}">add roadmap</a></li>
            <li><a href="{{ route('change_pkgs',[app()->getLocale(),0]) }}">change package</a></li>
            <li><a href="{{ route('change_invest_plans',[app()->getLocale(),0]) }}">invest Plans</a></li>
          </ul>
        </li>         
        @endif
      </ul>
    </section>
  </aside>