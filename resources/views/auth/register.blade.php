<!DOCTYPE html>
<html lang="en" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="images/favicon.png">
    <!-- Site Title  -->
    <title>Register To greenway &amp; CM</title>
    <!-- Bundle and Base CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.bundle.css?ver=200') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css?ver=200') }}">
    <!-- Extra CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css?ver=200') }}">
</head>

<body class="nk-body body-wider bg-light-alt">
    <div class="nk-wrap">
        <main class="nk-pages nk-pages-centered bg-theme">
            <div class="ath-container">
                <div class="ath-header text-center">
                    <a href="./" class="ath-logo"><img src="images/logo-full-white.png" srcset="images/logo-full-white2x.png 2x" alt="logo"></a>
                </div>
                <div class="ath-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <h5 class="ath-heading title">Sign Up <small class="tc-default">Create New TokenWiz Account</small></h5>
                     <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="field-item">
                            <div class="field-wrap">
                                <input input id="username" class="input-bordered" type="text" name="username" :value="old('username')" required autofocus autocomplete="username" placeholder="user name"/>
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input id="email" class="input-bordered" type="email" name="email" :value="old('email')" required placeholder="Your Email"/>
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input id="password" class="input-bordered" type="password" name="password" required autocomplete="new-password" placeholder="Password"/>
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input id="password_confirmation" class="input-bordered" type="password" name="password_confirmation" required autocomplete="new-password" placeholder="Repeat Password"/>
                            </div>
                        </div>
                        <div class="field-item">
                            <input class="input-checkboxs" name="terms" id="terms" type="checkbox">
                            <label for="agree-term-2">I agree to Greenway <a href="#">Privacy Policy</a> &amp; <a href="#">Terms</a>.</label>
                        </div>
                        <button class="btn btn-primary btn-block btn-md">Sign Up</button>
                    </form>
                    <div class="sap-text"><span>Or Sign Up With</span></div>
                    <ul class="btn-grp gutter-20px gutter-vr-20px">
                        <li class="col"><a href="#" class="btn btn-md btn-facebook btn-block"><em class="icon fab fa-facebook-f"></em><span>Facebook</span></a></li>
                        <li class="col"><a href="#" class="btn btn-md btn-google btn-block"><em class="icon fab fa-google"></em><span>Google</span></a></li>
                    </ul>
                </div>
                <div class="ath-note text-center tc-light"> Already have an account? <a href="{{ route('login') }}"> <strong>Sign in here</strong></a>
                </div>
            </div>
        </main>
    </div>
    <!-- Preloader -->
    <div class="preloader"><span class="spinner spinner-round"></span></div>
    <!-- JavaScript -->
    <script src="{{ asset('assets/js/jquery.bundle.js?ver=200') }}"></script>
    <script src="{{ asset('assets/js/scripts.js?ver=200') }}"></script>
    <script src="{{ asset('assets/js/charts.js?ver=200') }}"></script>
    <script src="{{ asset('assets/js/charts.js?ver=200') }}"></script>
</body>

</html>

