<!DOCTYPE html>
<html lang="en" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="images/favicon.png">
    <!-- Site Title  -->
    <title>Login To greenway &amp; CM</title>
    <!-- Bundle and Base CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.bundle.css?ver=200') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css?ver=200') }}">
    <!-- Extra CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css?ver=200') }}">
</head>

<body class="nk-body body-wider bg-light-alt">
    <div class="nk-wrap">
        <main class="nk-pages nk-pages-centered bg-theme">
            <div class="ath-container">
                <div class="ath-header text-center">
                    <a href="./" class="ath-logo"><img src="images/logo-full-white.png" srcset="images/logo-full-white2x.png 2x" alt="logo"></a>
                </div>
                <div class="ath-body">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <h5 class="ath-heading title">Sign in <small class="tc-default">with your ICO Account</small></h5>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="field-item">
                            <div class="field-wrap">
                                <label for="email" value="{{ __('Email') }}" />
                                <input id="email" class="input-bordered" type="email" name="email" :value="old('email')" required autofocus />
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <label for="password" value="{{ __('Password') }}" />
                                <input id="password" class="input-bordered" type="password" name="password" required autocomplete="current-password" />
                            </div>
                        </div>
                        <!-- <div class="captcha field-item">
                            <span>{!! captcha_img() !!}</span>
                            <button type="button" class="btn btn-danger" class="reload" id="reload">
                            ↻
                            </button>
                        </div>
                        <div class="form-group mb-4">
                            <input id="captcha" type="text" class="input-bordered" placeholder="Enter Captcha" name="captcha">
                        </div> -->

                        <div class="d-flex justify-content-between align-items-center pdb-r">
                            <div class="field-item pb-0">
                                <label for="remember_me" class="flex items-center">
                                    <checkbox id="remember_me" name="remember" class="input-checkbox"/>
                                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                </label>
                            </div>
                            <div class="forget-link fz-6">
                                 @if (Route::has('password.request'))
                                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                                        {{ __('Forgot your password?') }}
                                    </a>
                                @endif
                            </div>
                            
                        </div>
                         <button class="btn btn-primary btn-block btn-md">
                            {{ __('Log in') }}
                        </button>
                    </form>
                    <div class="sap-text"><span>Or Sign In With</span></div>
                    <ul class="row gutter-20px gutter-vr-20px">
                        <li class="col"><a href="#" class="btn btn-md btn-facebook btn-block"><em class="icon fab fa-facebook-f"></em><span>Facebook</span></a></li>
                        <li class="col"><a href="#" class="btn btn-md btn-google btn-block"><em class="icon fab fa-google"></em><span>Google</span></a></li>
                    </ul>
                </div>
                <div class="ath-note text-center tc-light"> Don’t have an account? <a href="{{ route('register') }}"> <strong>Sign up here</strong></a>
                </div>
            </div>
        </main>
    </div>
    <!-- Preloader -->
    <div class="preloader"><span class="spinner spinner-round"></span></div>
    <!-- JavaScript -->
    <script src="{{ asset('assets/js/jquery.bundle.js?ver=200') }}"></script>
    <script src="{{ asset('assets/js/scripts.js?ver=200') }}"></script>
    <script src="{{ asset('assets/js/charts.js?ver=200') }}"></script>
    <script src="{{ asset('assets/js/charts.js?ver=200') }}"></script>
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#reload').click(function () {
                    $.ajax({
                        type: 'GET',
                        url: 'reload-captcha',
                        success: function (data) {
                            $(".captcha span").html(data.captcha);
                        }
                    });
                });
            });
</script> -->
</body>
