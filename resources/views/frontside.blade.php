<!DOCTYPE html>
<html lang="en" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="{{ $page['site_name'] }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ $page['title'] }}">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <!-- Site Title  -->
    <title>{{ $page['title'] }}</title>
    <!-- Bundle and Base CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.bundle.css?ver=1930') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style-salvia.css?ver=1930') }}" id="changeTheme">
    <!-- Extra CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css?ver=1930') }}">

</head>
     

    <body class="nk-body body-wider mode-onepage">
        <div class="nk-wrap">
            <header class="nk-header page-header is-transparent is-sticky is-shrink is-split" id="header">
                <!-- Header @s -->
                <div class="header-main">
                    <div class="container container-xxl">
                        <div class="header-wrap">
                            <!-- Logo @s -->
                            <div class="header-logo logo animated" data-animate="fadeInDown" data-delay=".65">
                                <a href="./" class="logo-link">
                                    <img class="logo-dark" src="images/logo.png" srcset="images/logo2x.png 2x" alt="logo">
                                    <img class="logo-light" src="images/logo-full-white.png" srcset="images/logo-full-white2x.png 2x" alt="logo">
                                </a>
                            </div>

                            <!-- Menu Toogle @s -->
                            <div class="header-nav-toggle">
                                <a href="#" class="navbar-toggle" data-menu-toggle="header-menu">
                                    <div class="toggle-line">
                                        <span></span>
                                    </div>
                                </a>
                            </div>

                            <!-- Menu @s -->
                            <div class="header-navbar header-navbar-s2 flex-grow-1 animated" data-animate="fadeInDown" data-delay=".75">
                                <nav class="header-menu header-menu-s2" id="header-menu">
                                    <ul class="menu mx-auto">
                                    <li class="menu-item">
                                        <a class="menu-link nav-link menu-toggle" href="#home">Home</a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="menu-link nav-link menu-toggle" href="#why">{{ $page['blockT'] }}</a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="menu-link nav-link menu-toggle" href="#roadmap">{{ $page['roadT'] }}</a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="menu-link nav-link menu-toggle" href="#docs">{{ $page['docsT'] }}</a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="menu-link nav-link menu-toggle" href="#contact">Contact us</a>
                                    </li>
                                </ul>

                                <ul class="menu-btns">
                                    <li><a href="{{ route('register') }}" class="btn btn-md btn-auto btn-secondary btn-outline no-change"><span>Sign Up</span></a></li>
                                    <li><a href="{{ route('login') }}" class="btn btn-md btn-auto btn-secondary no-change focus"><span>Login</span></a></li>
                                </ul>
                            </nav>
                            </div><!-- .header-navbar @e -->
                        </div>                                                
                    </div>
                </div><!-- .header-main @e -->
                <!-- Banner @s -->
                <div class="header-banner bg-theme-grad-s2" id="home">
                    <div class="nk-banner">
                        <div class="banner banner-fs banner-single banner-s1">
                            <div class="banner-wrap my-auto">
                                <div class="container container-xxl">
                                    <div class="row align-items-center justify-content-center justify-content-lg-between gutter-vr-60px">
                                        <div class="col-lg-6 col-xl-5 text-center text-lg-left">
                                            <div class="banner-caption tc-light animated" data-animate="fadeInUp" data-delay="1.25">
                                                <div class="cpn-head mt-0">
                                                    <h1 class="title title-lg-s2">{{ $page['bannerT'] }}</h1>
                                                </div>
                                                <div class="cpn-text cpn-text-s3 pb-2">
                                                    <p class="lead lead-s3 pb-1">{{ $page['bannerD'] }}</p>
                                                </div>
                                                <div class="cpn-btns">
                                                    <ul class="btn-grp">
                                                        <li><a class="btn btn-lg btn-secondary" href="#">{{ $page['bannerB1'] }}</a></li>
                                                        <li><a class="btn btn-lg btn-primary btn-outline" href="#">{{ $page['bannerB2'] }}</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!-- .col -->
                                        <div class="col-lg-6 col-xl-7 col-md-8 col-sm-9">
                                            <div class="banner-gfx banner-gfx-s2 position-relative animated" data-animate="fadeInUp" data-delay="1.35">
                                                <img src="images/app-screens/sc-mockup.png" alt="mockup">
                                                <div class="gfx-slider gfx-screen round" data-slide-speed="2500" data-slide-show="true" data-anim-loop="true" data-anim-speed="1500">
                                                    <ul class="slides">
                                                        <li class="gfx-slide">
                                                            <div class="bg-image round">
                                                                <img src="{{ asset('images/app-screens/sc-slide-one.jpg') }}" alt="slide img">
                                                            </div>
                                                        </li>
                                                        <li class="gfx-slide">
                                                            <div class="bg-image round">
                                                                <img src="{{ asset('images/app-screens/sc-slide-two.jpg') }}" alt="slide img">
                                                            </div>
                                                        </li>
                                                        <li class="gfx-slide">
                                                            <div class="bg-image round">
                                                                <img src="{{ asset('images/app-screens/sc-slide-three.jpg') }}" alt="slide img">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!-- .col -->
                                    </div><!-- .row -->
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-banner -->
                    <div class="nk-ovm shape-z6">
                        <div class="nk-ovm-inner"></div>
                    </div>
                </div>
                <!-- .header-banner @e -->
            </header>
        
            <main class="nk-pages">
                 @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif

            @error('name')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
                 <section class="section bg-white" id="why">
                    <div class="ui-shape ui-shape-s1"></div>
                    <div class="container">
                        <!-- Block @s -->
                        <div class="nk-block nk-block-about">
                            <div class="row align-items-center gutter-vr-30px">
                                <div class="col-lg-5 text-center text-lg-left">
                                    <div class="nk-block-text">
                                        <h2 class="title title-semibold animated" data-animate="fadeInUp" data-delay=".1">{{ $page['blockT'] }}</h2>
                                        <p class="tc-dark animated" data-animate="fadeInUp" data-delay=".2">{{ $page['blockD'] }}</p>
                                    </div>
                                </div><!-- .col -->
                                <div class="col-lg-7">
                                    <div class="nk-block-img nk-block-img-s2 text-center text-lg-left animated" data-animate="fadeInUp" data-delay=".5">
                                        <img src="images/zinnia/gfx-f.png" alt="">
                                    </div>
                                </div><!-- .col -->
                            </div><!-- .row -->
                        </div><!-- .block @e -->
                    </div>
                </section>
                <!-- // -->
                
                <!-- // -->
                
                <section class="section bg-theme tc-light ov-h" id="roadmap">
                    <div class="container">
                        <!-- Section Head @s -->
                        <div class="section-head text-center wide-auto-sm">
                            <h4 class="title title-semibold animated" data-animate="fadeInUp" data-delay=".1">{{ $page['roadT'] }}</h4>
                            <p class="animated" data-animate="fadeInUp" data-delay=".2">{{ $page['roadD'] }} </p>
                        </div><!-- .section-head @e -->
                        <!-- Block @s -->
                        <div class="nk-block">
                            <div class="row justify-content-center">
                                <div class="col-xl-12">
                                    <div class="roadmap-all mgb-m50 animated" data-animate="fadeInUp" data-delay=".3">
                                        
                                        <!-- <div class="roadmap roadmap-current roadmap-s1 text-lg-center"> -->
                                        <div class="roadmap-wrap roadmap-wrap-done roadmap-wrap-s1 mb-0">
                                            <div class="row no-gutters">
                                                @for($i=0 ; $i<4 ; $i++)
                                                @if(isset($roadmap[$i]))
                                                <div class="col-lg">
                                                    <div class="roadmap roadmap-s1 roadmap-done text-lg-center">
                                                        <div class="roadmap-step roadmap-step-s1">
                                                            <div class="roadmap-head roadmap-head-s1">
                                                                <span class="roadmap-time roadmap-time-s1">{{$roadmap[$i]->dat}}</span>
                                                                <span class="roadmap-title roadmap-title-s1">{{$roadmap[$i]->tit}}</span>
                                                            </div>
                                                            <ul class="roadmap-step-list roadmap-step-list-s1">
                                                                <li>{{$roadmap[$i]->des}}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                @endif
                                                @endfor
                                            </div><!-- .row -->
                                        </div>
                                        

                                        @if(isset($roadmap[4]))

                                        <div class="roadmap-wrap roadmap-wrap-done roadmap-wrap-s1 mb-0">
                                            <div class="row flex-row-reverse no-gutters">
                                                @for($i=4 ; $i<7 ; $i++)
                                                @if(isset($roadmap[$i]))
                                                <div class="col-lg">
                                                    <div class="roadmap roadmap-done roadmap-s1 text-lg-center">
                                                        <div class="roadmap-step roadmap-step-s1">
                                                            <div class="roadmap-head roadmap-head-s1">
                                                                <span class="roadmap-time roadmap-time-s1">{{$roadmap[$i]->dat}}</span>
                                                                <span class="roadmap-title roadmap-title-s1">{{$roadmap[$i]->tit}}</span>
                                                            </div>
                                                            <ul class="roadmap-step-list roadmap-step-list-s1">
                                                                <li>{{$roadmap[$i]->des}}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                @endif
                                                @endfor
                                            </div><!-- .row -->
                                        </div>

                                        @endif
                                        
                                        @if(isset($roadmap[7]))
                                        <div class="roadmap-wrap roadmap-wrap-s1 mb-lg-0">
                                            <div class="row no-gutters">
                                                
                                                @for($i=7 ; $i<10 ; $i++)
                                                @if(isset($roadmap[$i]))
                                                <div class="col-lg">
                                                    <div class="roadmap roadmap-done roadmap-s1 text-lg-center">
                                                        <div class="roadmap-step roadmap-step-s1">
                                                            <div class="roadmap-head roadmap-head-s1">
                                                                <span class="roadmap-time roadmap-time-s1">{{$roadmap[$i]->dat}}</span>
                                                                <span class="roadmap-title roadmap-title-s1">{{$roadmap[$i]->tit}}</span>
                                                            </div>
                                                            <ul class="roadmap-step-list roadmap-step-list-s1">
                                                                <li>{{$roadmap[$i]->des}}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                 @endif
                                                @endfor
                                                
                                            </div><!-- .row -->
                                        </div>
                                        @endif


                                        @if(isset($roadmap[10]))

                                        <div class="roadmap-wrap roadmap-wrap-s1 mb-0">
                                            <div class="row flex-row-reverse no-gutters">
                                                @for($i=10 ; $i<13 ; $i++)
                                                @if(isset($roadmap[$i]))
                                                <div class="col-lg">
                                                    <div class="roadmap roadmap-current roadmap-s1 text-lg-center">
                                                        <div class="roadmap-step roadmap-step-s1">
                                                            <div class="roadmap-head roadmap-head-s1">
                                                                <span class="roadmap-time roadmap-time-s1">{{$roadmap[$i]->dat}}</span>
                                                                <span class="roadmap-title roadmap-title-s1">{{$roadmap[$i]->tit}}</span>
                                                            </div>
                                                            <ul class="roadmap-step-list roadmap-step-list-s1">
                                                                <li>{{$roadmap[$i]->des}}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                @endif
                                                @endfor
                                            </div><!-- .row -->
                                        </div>

                                        @endif

                                    </div>
                                </div><!-- .col -->
                            </div><!-- .row -->
                        </div><!-- .block @e -->
                    </div>
                    <div class="nk-ovm shape-n"></div>
                </section>
                <!-- // -->
           


                <section class="bg-white ov-h section" id="docs">

                    <div class="container">
                        <div class="row justify-content-center text-center">
                            <div class="col-lg-6">
                                <div class="section-head section-head-s2">
                                    <h2 class="title title-xl animated" data-animate="fadeInUp" data-delay="0.1" title="Downloads">{{ $page['docsT'] }}</h2>
                                    <p class="animated" data-animate="fadeInUp" data-delay="0.2">{{ $page['docsT'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container container-xxl">
                        <div class="nk-block">
                            <div class="row gutter-vr-50px">

                                @foreach($docs as $doc)
                                <div class="col-sm-6 col-lg-3">
                                    <div class="doc animated" data-animate="fadeInUp" data-delay="0.3">
                                        <div class="doc-photo doc-shape doc-shape-a">
                                            <img src="images/azalea/{{$doc->image}}" alt="">
                                        </div>
                                        <div class="doc-text">
                                            <h5 class="doc-title title-sm">{{$doc->name}} <small>({{$doc->year}})</small></h5>
                                            <a class="doc-download" href="#"><em class="ti ti-import"></em></a>
                                            <div class="doc-lang">ENGLISH</div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>

                </section>
                <!-- // -->
                 <section class="section bg-theme tc-light" id="contact">

                    <div class="container">
                        <!-- Block @s -->
                        <div class="nk-block nk-block-about">
                            <div class="row justify-content-between align-items-center gutter-vr-50px">
                                <div class="col-lg-6">
                                    <div class="nk-block-text">
                                        <div class="nk-block-text-head">
                                            <h2 class="title title-lg ttu animated" data-animate="fadeInUp" data-delay="0.7">{{ $page['contactT'] }}</h2>
                                            <p class="animated" data-animate="fadeInUp" data-delay="0.8">{{ $page['contactT'] }}</p>
                                        </div>
                                        <form class="nk-form-submit" action="form/contact.php" method="post">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="field-item animated" data-animate="fadeInUp" data-delay="0.8">
                                                        <label class="field-label ttu">Your Name</label>
                                                        <div class="field-wrap">
                                                            <input name="contact-name" placeholder="Introduce yourself" type="text" class="input-bordered required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="field-item animated" data-animate="fadeInUp" data-delay="0.9">
                                                        <label class="field-label ttu">Your Email</label>
                                                        <div class="field-wrap">
                                                            <input name="contact-email" placeholder="Who do we replay to" type="email" class="input-bordered required email">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field-item animated" data-animate="fadeInUp" data-delay="1.0">
                                                <label class="field-label ttu">Your Message</label>
                                                <div class="field-wrap">
                                                    <textarea name="contact-message" placeholder="Leave your question or comment here" class="input-bordered input-textarea required"></textarea>
                                                </div>
                                            </div>
                                            <input type="text" class="d-none" name="form-anti-honeypot" value="">
                                            <div class="row">
                                                <div class="col-sm-5 text-right animated" data-animate="fadeInUp" data-delay="1.1">
                                                    <button type="submit" class="btn btn-round btn-primary">SEND</button>
                                                </div>
                                                <div class="col-sm-7 order-sm-first">
                                                    <div class="form-results"></div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-5 text-center order-lg-first">
                                    <div class="nk-block-contact nk-block-contact-s1  animated" data-animate="fadeInUp" data-delay="0.1">
                                        <ul class="contact-list">
                                            <li class="animated" data-animate="fadeInUp" data-delay="0.2">
                                                <em class="contact-icon fas fa-phone"></em>
                                                <div class="contact-text">
                                                    <span>{{ $page['mobile'] }}</span>
                                                </div>
                                            </li>
                                            <li class="animated" data-animate="fadeInUp" data-delay="0.3">
                                                <em class="contact-icon fas fa-envelope"></em>
                                                <div class="contact-text">
                                                    <span>{{ $page['mail'] }}</span>
                                                </div>
                                            </li>
                                            <li class="animated" data-animate="fadeInUp" data-delay="0.4">
                                                <em class="contact-icon fas fa-paper-plane"></em>
                                                <div class="contact-text">
                                                    <span>Join us on Telegram</span>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="nk-circle-animation nk-df-center white small"></div><!-- .circle-animation -->
                                    </div>
                                    
                                    <ul class="social-links social-links-s2 justify-content-center animated" data-animate="fadeInUp" data-delay="0.6">
                                        <li><a href="#"><em class="fab fa-twitter"></em></a></li>
                                        <li><a href="#"><em class="fab fa-medium-m"></em></a></li>
                                        <li><a href="#"><em class="fab fa-facebook-f"></em></a></li>
                                        <li><a href="#"><em class="fab fa-youtube"></em></a></li>
                                        <li><a href="#"><em class="fab fa-bitcoin"></em></a></li>
                                        <li><a href="#"><em class="fab fa-github"></em></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .block @e -->
                    </div>
                </section>
                <!-- // -->
            </main>
            
            <footer class="nk-footer bg-theme ov-h">
                <div class="section section-m footer-bottom animated" data-animate="fadeInUp" data-delay="0.5">
                    <div class="container">
                        <div class="row justify-content-md-between align-items-center">
                            <div class="col-lg-6 col-md-3 col-sm-4">
                                <a href="./" class="wgs-logo-s2 d-inline-block mb-2 mb-md-0">
                                    <img src="images/logo-full-white.png" srcset="images/logo-full-white2x.png 2x" alt="logo">
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-8">
                                <div class="copyright-text">
                                    <ul class="d-flex justify-content-between align-items-center flex-wrap flex-md-nowrap">
                                        <li><a href="#">User Agreement</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                        <li><p>©2021 - {{ $page['site_name'] }}. All rights reserved</p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nk-ovm shape-z7 ov-h"></div>
            </footer>
        </div>
        
         <!-- Modal @s -->
        <div class="modal fade" id="login-popup">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="ath-container m-0">
                        <div class="ath-body bg-theme tc-light">
                            <h5 class="ath-heading title">Sign in <small class="tc-default">with your ICO Account</small></h5>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="text" class="input-bordered" name="email" :value="old('email')" placeholder="Your Email">
                                    </div>
                                </div>
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="password" class="input-bordered" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <div class="captcha">
                                            <span>{!! captcha_img() !!}</span>
                                                <button type="button" class="btn btn-danger" class="reload" id="reload">
                                                ↻
                                                </button>
                                        </div>
                                    </div>
                                    <input id="captcha" type="text" class="input-bordered" placeholder="Enter Captcha" name="captcha" />
                                </div>
                                <div class="field-item d-flex justify-content-between align-items-center">
                                    <div class="field-item pb-0">
                                        <input class="input-checkbox" id="remember-me-100" type="checkbox">
                                        <label for="remember-me-100">Remember Me</label>
                                    </div>
                                    <div class="forget-link fz-6">
                                        <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#reset-popup">Forgot password?</a>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block btn-md">Sign In</button>
                            </form>
                            
                            <div class="ath-note text-center">
                                Don’t have an account? <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#register-popup"> <strong>Sign up here</strong></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .modal @e -->
        
        <!-- Modal @s -->
        <div class="modal fade" id="register-popup">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="ath-container m-0">
                        <div class="ath-body bg-theme tc-light">
                            <h5 class="ath-heading title">Sign Up <small class="tc-default">Create New TokenWiz Account</small></h5>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="text" class="input-bordered" placeholder="User Name" name="username" :value="old('username')" required autofocus autocomplete="username" />
                                    </div>
                                </div>
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="email" class="input-bordered" placeholder="Your Email" name="email" :value="old('email')" required  />
                                    </div>
                                </div>
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="password" class="input-bordered" placeholder="Password" name="password" required autocomplete="new-password" />
                                    </div>
                                </div>
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="password" class="input-bordered" placeholder="Repeat Password" name="password_confirmation" required autocomplete="new-password" />
                                    </div>
                                </div>
                                <div class="field-item">
                                    <input class="input-checkbox" name="terms" id="terms" type="checkbox">
                                    <label for="agree-term-2">I agree to Icos <a href="#">Privacy Policy</a> &amp; <a href="#">Terms</a>.</label>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block btn-md">Sign Up</button>
                            </form>
                            <div class="ath-note text-center">
                                Already have an account? <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#login-popup"> <strong>Sign in here</strong></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .modal @e -->
        
        <!-- Modal @s -->
        <div class="modal fade" id="reset-popup">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                    <div class="ath-container m-0">
                        <div class="ath-body bg-theme tc-light">
                            <h5 class="ath-heading title">Reset <small class="tc-default">with your Email</small></h5>
                            <form action="#">
                                <div class="field-item">
                                    <div class="field-wrap">
                                        <input type="text" class="input-bordered" placeholder="Your Email">
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-block btn-md">Reset Password</button>
                                <div class="ath-note text-center">
                                    Remembered? <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#login-popup"> <strong>Sign in here</strong></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .modal @e -->
        
        <!-- preloader -->
        <div class="preloader preloader-alt no-split"><span class="spinner spinner-alt"><img class="spinner-brand" src="images/logo-full-white.png" alt=""></span>
        </div>
        
        <!-- JavaScript -->
        <script src="{{ asset('assets/js/jquery.bundle.js?ver=1930') }}"></script>
        <script src="{{ asset('assets/js/scripts.js?ver=1930') }}"></script>
        <script src="{{ asset('assets/js/charts.js?var=161') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#reload').click(function () {
                    $.ajax({
                        type: 'GET',
                        url: 'reload-captcha',
                        success: function (data) {
                            $(".captcha span").html(data.captcha);
                        }
                    });
                });
            });
        </script>

    </body>

</html>