 <x-app-layout>
	
    @push('styles')
        	<!--alerts CSS -->
    <link href="{{ asset('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
    	.go {
				  color: green;
				}
				.stop {
				  color: red;
				}
    </style>
    @endpush
    @push('breadcrumb')
    <h1>
        Invest
        <small>invest calculator</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Invest Plans</a></li>
        <li class="breadcrumb-item active">invest calculator</li>
      </ol>
      @endpush

@if(Session::has('success'))
        <div class="alert alert-success text-center h3">
            {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger text-center h3">
            {{ Session::get('error') }}
        </div>
        @endif
        
     <div class="box box-inverse bg-dark bg-hexagons-white">
			<div class="box-body">
				<h1 class="page-header text-center no-border font-weight-600 font-size-60 mt-25"><span class="text-yellow">Buy</span> and <span class="text-danger">Invest</span> With us<br> without additional fees</h1>
				<h3 class="subtitle text-center ">your<span class="text-danger"> ZOOMS </span> profit From {{ $user['pkgs']['invest']}}% upTo {{ $user['pkgs']['invest']+10}}% </h3>
				<h3 class="subtitle text-center ">your<span class="text-warning"> Package </span> profit From {{ $user['pkgs']['invest_count']-10}}% upTo {{ $user['pkgs']['invest_count']}}% </h3>
				<div class="row">
					<div class="col-12">
						<div class="exchange-calculator text-center mt-35">
							<input type="number" class="form-control" id="invest-amount" placeholder="" value="250">                
							<select class="coins-exchange" name="state">
							   <option value="USDT">USDT</option>
							   <option value="BTC" disabled="">BTC</option>
							   <option value="BTC" disabled="">Ethereum</option>
							   <option value="Ripple" disabled="">Ripple</option>
							   <option value="Ripple" disabled="">Bitcoin Cash</option>
							   <option value="Ripple" disabled="">Cardano</option>
							   <option value="Ripple" disabled="">Litecoin</option>
							   <option value="Ripple" disabled="">NEO</option>
							   <option value="Ripple" disabled="">BNB</option>
							   <option value="Ripple" disabled="">EOS</option>
							   <option value="Ripple" disabled="">NEM</option>
							</select>
							
							<a href="#" onClick="calculateInvestRate()" class="btn btn-yellow btn-lg mx-auto">calculate invest profit</a>
					   </div>
					</div>
				</div>
				<div class="col-12" id="inv_res">
						<div class="exchange-calculator text-center mt-35">
							<label class="text-danger h4">Basic Package profit </label>
							
							<div class="equal"> = </div>
							<input type="text" class="form-control" id="invest-profit" placeholder="" value="0" disabled="">                
							
					   <br>
							<label class="text-warning h4">ZOOMS ADDED profit </label>
							
							<div class="equal"> = </div>
							<input type="text" class="form-control" id="zooms-profit" placeholder="" value="0" disabled="">                
							
					   </div>

					   <div class="exchange-calculator text-center mt-35">
							<h3>TOTAL AVG PROFIT </h3>
							
							<input type="text" class="form-control" id="total-profit" placeholder="" value="0" disabled="">                
							
					   </div>
					</div>

				</div>
			</div>
		

<script src="https://widgets.coingecko.com/coingecko-coin-price-chart-widget.js"></script>
<coingecko-coin-price-chart-widget  coin-id="bitcoin" currency="usd" height="300" locale="en"></coingecko-coin-price-chart-widget>

<div data-background-color="" data-currency="usd" data-coin-id="eth2-staking-by-poolx" data-locale="en" data-height="300" data-width="" class="coingecko-coin-price-chart-widget"></div>
<script src="https://widgets.coingecko.com/div/coingecko-coin-price-chart-widget-div.js"></script>

		<div class="box box-solid bg-dark">
        <div class="box-header with-border">
          <h3 class="box-title">RealTime Project Capitalizations</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
			<div class="table-responsive">
				<table class="table table-bordered dataTable no-footer" id="dataTable_crypto" role="grid" width="100%">
                  <thead>
                     <tr role="row">
                        <th colspan="2" rowspan="1">Country/Project</th>
                        <th class="text-right">EST</th>
                        <th class="text-right">Invest Est</th>
                        <th class="text-right">Pool Stats</th>
                        <th class="text-right">Status</th>
                        <th class="text-right">Start</th>
                        <th class="text-right">End</th>
                        <th class="text-right">Change</th>
                     </tr>
                  </thead>
                   <tbody>
                   	@foreach($invest_plans as $invP)
                      <tr role="row">
                         <td><span><a href="#"><img src="{{ asset('img/default/flag/'.strtolower($invP['it_country']['iso']).'.svg')}}" width="30"></a></span></td>
                         <td>
                            <small><a href="#" class="text-yellow hover-warning"> {{ $invP->currency }}</a></small>
                            <h6 class="text-muted">{{ $invP->title_small }}</h6>
                         </td>
                         <td class="text-right"><p><span>$</span> {{ number_format($invP->volume) }}</p></td>
                         <td class="text-right"><div class="progress progress-sm"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: {{ $invP->invest_cap }}%"></div></div></td>
                         <td class="text-right"><a href="javascript:void(0)">
                         	<img id="block-{{$invP->block}}-1" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="away" width="25" /> 
                         	<img id="block-{{$invP->block}}-2" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="online" width="25" /> 
                         	<img id="block-{{$invP->block}}-3" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="busy" width="25" />
                         	<img id="block-{{$invP->block}}-4" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="online" width="25" /> 
                         	<img id="block-{{$invP->block}}-5" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="away" width="25" /> 
                         	<img id="block-{{$invP->block}}-5" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="busy" width="25" /> 
                         </a></td>
                         <td class="no-wrap text-right"><label class="label label-success">{{ $invP->status }}</label></td>
                         <td class="no-wrap text-right">{{ $invP->start }}</td>
                         <td class="no-wrap text-right">{{ $invP->end }}</td>
                         <td><label  id="my_box{{ $invP->id }}" class="label label-success"><i id="i-{{$invP->id}}" class="fa"></i> <span id="val-{{$invP->id}}">{{ mt_rand($invP->change-3 , $invP->change+3) }}.0{{ rand(0 , 9) }}%</span></label></td>
                      </tr>
                      @endforeach
                   </tbody>
                </table>
			</div>
        </div>
        
      </div>


       @push('scripts')
           <!-- SlimScroll -->
					<script src="{{ asset('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
					
					<!-- This is data table -->
				    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
					
					<!-- Sparkline -->
					<script src="{{ asset('assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
					
					<!-- Crypto_Admin for demo purposes -->
					<script src="{{ asset('js/pages/market-capitalizations.js') }}"></script>
    			
    			<script type="text/javascript">
    					$( document ).ready(function() {
    						document.getElementById("inv_res").style.display = "none";
    						var CountPro = {{ count($invest_plans) }};
    						let nIntervId;
									  
									  if (!nIntervId) {
									    nIntervId = setInterval(flashText, 2000);
									  }

									function flashText() {
										const value6 = generateRandomFloatInRange(2.5, 5.75);
										const ii = randomIntFromInterval(1, CountPro);
										const yy = randomIntFromInterval(1, 5);
									  const oElem = document.getElementById("my_box"+ii);
									  const oElS = document.getElementById("i-"+ii);
									  const oElV = document.getElementById("val-"+ii);
									  const oElB = document.getElementById("block-"+ii+"-"+yy);
									  
									  if (oElem.className === "go") {
									    oElem.className = "stop";
									    oElS.className = "fa fa-chevron-down";
									    oElV.innerText=value6.toFixed(2)+'%';
									    oElB.style.display = "block";
									  } else {
									    oElem.className = "go";
									    oElS.className = "fa fa-chevron-up";
									    oElV.innerText=value6.toFixed(2)+'%';
									    oElB.style.display = "none";
									  }
									}

							});

							function randomIntFromInterval(min, max) { // min and max included 
							  return Math.floor(Math.random() * (max - min + 1) + min)
							}

							function generateRandomFloatInRange(min, max) {
							    return (Math.random() * (max - min + 1)) + min;
							}

							

							
    			</script>

    			 <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/vendor_components/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
    <script type="text/javascript">
    	function calculateInvestRate() {
								const invAmount = document.getElementById("invest-amount");
								const basProfit = document.getElementById("invest-profit");
								const zomProfit = document.getElementById("zooms-profit");
								const totProfit = document.getElementById("total-profit");

								if(invAmount.value < 250){
													 swal({   
										            title: "Invest Must Be More Than 250$",   
										            text: "This is Auto close alert!",
										            timer: 3000,   
										            showConfirmButton: false 
										        });
								}else{
									var InvZoms = {{$user['pkgs']['invest']}};
									var InvRate = {{$user['pkgs']['invest_count']}};

									basProfit.value = parseInt(invAmount.value * InvRate * 0.01 );
									zomProfit.value = parseInt(invAmount.value * InvZoms * 0.01 );
									totProfit.value = parseInt(basProfit.value) + parseInt(zomProfit.value);
									
									
									document.getElementById("inv_res").style.display = "block";
									
								}
								
							}
    </script>
        @endpush
</x-app-layout>                