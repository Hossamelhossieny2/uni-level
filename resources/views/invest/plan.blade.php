 <x-app-layout>
	
    @push('styles')
        	<!--alerts CSS -->
    <link href="{{ asset('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
    	.go {
				  color: green;
				}
				.stop {
				  color: red;
				}
    </style>
    @endpush
    @push('breadcrumb')
    <h1>
        Invest Plans
        <small>{{ $plan->title }}</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Invest Plans</a></li>
        <li class="breadcrumb-item active">{{ $plan->title }}</li>
      </ol>
      @endpush
@foreach ($errors->all() as $error)
                <li style="list-style: none;text-align: center;"><div class="alert alert-danger">{{ $error }}</div></li>
            @endforeach

@if(Session::has('success'))
        <div class="alert alert-success text-center h3">
            {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger text-center h3">
            {{ Session::get('error') }}
        </div>
        @endif
 
 <div class="row">
 <div class="col-12">
		<div class="box box-inverse bg-dark bg-hexagons-white">
			<div class="box-body">
				<h4 class="page-header text-center no-border font-weight-200 font-size-30"><span class="text-yellow">Invest</span> in <span class="text-danger"> <img src="{{ asset('img/default/flag/'.strtolower($plan['it_country']['iso']).'.svg') }}" width="40"> {{ $plan->title }}</span> Now <i class="cc {{$plan->sign}} mr-5" title="{{$plan->sign}}"></i></h4>

				<div class="row">
					<div class="col-12">
						<form method="post" class="form-group" action="{{ route('buy_invest',app()->getLocale()) }}">
							@csrf
						<div class="exchange-calculator text-center mt-35">
							<label>invest amount:</label>
							<input type="number" class="form-control" name="invest_amount" placeholder="" min="250" style="width: 100px;height: 50px;" />                
							<select class="coins-exchange" name="state" class="form-control">
							   <option value="USDT">USDT</option>
							   <option value="BTC" disabled="">BTC</option>
							   <option value="BTC" disabled="">Ethereum</option>
							   <option value="Ripple" disabled="">Ripple</option>
							   <option value="Ripple" disabled="">Bitcoin Cash</option>
							   <option value="Ripple" disabled="">Cardano</option>
							   <option value="Ripple" disabled="">Litecoin</option>
							   <option value="Ripple" disabled="">NEO</option>
							   <option value="Ripple" disabled="">BNB</option>
							   <option value="Ripple" disabled="">EOS</option>
							   <option value="Ripple" disabled="">NEM</option>
							</select>
							<label>trans pass:</label>
							<input type="password" class="form-control" placeholder="" name="trans_pass" placeholder="transaction password here" style="width: 100px;height: 50px;"/>
							<input type="hidden" name="invest_plan" value="{{ $plan->id }}" />  
							<button type="submit" class="btn btn-success btn-lg mx-auto">invest Now</button>
					   </div>
					   </form>
					</div>
				</div>
				

				</div>
		</div>


<div class="row">
<div class="col-12">      
@if($plan->sign == 'BTC')
<script src="https://widgets.coingecko.com/coingecko-coin-price-chart-widget.js"></script>
<coingecko-coin-price-chart-widget  coin-id="bitcoin" currency="usd" height="300" locale="en"></coingecko-coin-price-chart-widget>
@elseif($plan->sign == 'ETH')
<div data-background-color="" data-currency="usd" data-coin-id="eth2-staking-by-poolx" data-locale="en" data-height="300" data-width="" class="coingecko-coin-price-chart-widget"></div>
<script src="https://widgets.coingecko.com/div/coingecko-coin-price-chart-widget-div.js"></script>
@endif
</div>
</div>


		
		
</div>
</div>

<div class="col-12">
            <div class="box">

              <div class="row no-gutters">
                <a class="col-4 bg-img d-none d-md-block" style="background-image: url({{ asset($plan->image) }})" href="#"></a>

                <div class="col-md-8">
                  <div class="box-body">
                    <h4><a href="#">{{ $plan->title }}</a></h4>
                    <hr class="w-50 ml-0 bt-2">

                    <p>{{ $plan->desc }}</p>

                    <div class="flexbox align-items-center mt-3">
                      <a class="text-gray" href="#">
                        Coin : <i class="cc {{ $plan->sign }} mr-5" title="{{ $plan->sign }}"></i>
                        <span class="ml-2">Country : <img src="{{ asset('img/default/flag/'.strtolower($plan['it_country']['iso']).'.svg') }}" width="40"></span>
                      </a>
<!-- <img class="card-img-top img-responsive" src="{{ asset($plan->image) }}" alt="Card image cap"> -->
                      <a href="#">
                        <i class="ion-heart text-danger font-size-11"></i>
                        <span class="font-size-11 text-fade ml-1">{{$plan->members+100}}</span>
                      </a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

<div class="row">
 <div class="col-12">
		<div class="box box-solid bg-dark">
	        <div class="box-header with-border">
	          <h3 class="box-title">RealTime Project Capitalizations</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
	                    title="Collapse">
	              <i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
	              <i class="fa fa-times"></i></button>
	          </div>
	        </div>
	        <div class="box-body">
				<div class="table-responsive">
					<table class="table table-bordered dataTable no-footer" id="dataTable_crypto" role="grid" width="100%">
	                  <thead>
	                     <tr role="row">
	                        <th colspan="2" rowspan="1">Country/Project</th>
	                        <th class="text-right">EST</th>
	                        <th class="text-right">Invest Est</th>
	                        <th class="text-right">Pool Stats</th>
	                        <th class="text-right">Status</th>
	                        <th class="text-right">Start</th>
	                        <th class="text-right">End</th>
	                        <th class="text-right">Change</th>
	                     </tr>
	                  </thead>
	                   <tbody>
	                      <tr role="row">
	                         <td><span><a href="#"><img src="{{ asset('img/default/flag/'.strtolower($plan['it_country']['iso']).'.svg')}}" width="30"></a></span></td>
	                         <td>
	                            <small><a href="#" class="text-yellow hover-warning"> {{ $plan->currency }}</a></small>
	                            <h6 class="text-muted">{{ $plan->title_small }}</h6>
	                         </td>
	                         <td class="text-right"><p><span>$</span> {{ number_format($plan->volume) }}</p></td>
	                         <td class="text-right"><div class="progress progress-sm"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: {{ $plan->invest_cap }}%"></div></div></td>
	                         <td class="text-right"><a href="javascript:void(0)">
	                         	<img id="block-{{$plan->block}}-1" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="away" width="25" /> 
	                         	<img id="block-{{$plan->block}}-2" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="online" width="25" /> 
	                         	<img id="block-{{$plan->block}}-3" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="busy" width="25" />
	                         	<img id="block-{{$plan->block}}-4" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="online" width="25" /> 
	                         	<img id="block-{{$plan->block}}-5" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="away" width="25" /> 
	                         	<img id="block-{{$plan->block}}-5" style="display: block;float: left;width: 20px;height:10px" src="{{ asset('images/avatar/2.jpg') }}" class="busy" width="25" /> 
	                         </a></td>
	                         <td class="no-wrap text-right"><label class="label label-success">{{ $plan->status }}</label></td>
	                         <td class="no-wrap text-right">{{ $plan->start }}</td>
	                         <td class="no-wrap text-right">{{ $plan->end }}</td>
	                         <td><label  id="my_box{{ $plan->id }}" class="label label-success"><i id="i-{{$plan->id}}" class="fa"></i> <span id="val-{{$plan->id}}">{{ mt_rand($plan->change-3 , $plan->change+3) }}.0{{ rand(0 , 9) }}%</span></label></td>
	                      </tr>
	                   </tbody>
	                </table>
				</div>
	        </div>
      	</div>
</div>
</div>
       @push('scripts')
           <!-- SlimScroll -->
					<script src="{{ asset('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
					
					<!-- This is data table -->
				    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
					
					<!-- Sparkline -->
					<script src="{{ asset('assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
					
					<!-- Crypto_Admin for demo purposes -->
					<script src="{{ asset('js/pages/market-capitalizations.js') }}"></script>
    			
    			<script type="text/javascript">
    					$( document ).ready(function() {
    						var CountPro = {{ count($invest_plans) }};
    						let nIntervId;
									  
									  if (!nIntervId) {
									    nIntervId = setInterval(flashText, 2000);
									  }

									function flashText() {
										const value6 = generateRandomFloatInRange(2.5, 5.75);
										const ii = randomIntFromInterval(1, CountPro);
										const yy = randomIntFromInterval(1, 5);
									  const oElem = document.getElementById("my_box"+ii);
									  const oElS = document.getElementById("i-"+ii);
									  const oElV = document.getElementById("val-"+ii);
									  const oElB = document.getElementById("block-"+ii+"-"+yy);
									  
									  if (oElem.className === "go") {
									    oElem.className = "stop";
									    oElS.className = "fa fa-chevron-down";
									    oElV.innerText=value6.toFixed(2)+'%';
									    oElB.style.display = "block";
									  } else {
									    oElem.className = "go";
									    oElS.className = "fa fa-chevron-up";
									    oElV.innerText=value6.toFixed(2)+'%';
									    oElB.style.display = "none";
									  }
									}
							});

							function randomIntFromInterval(min, max) { // min and max included 
							  return Math.floor(Math.random() * (max - min + 1) + min)
							}

							function generateRandomFloatInRange(min, max) {
							    return (Math.random() * (max - min + 1)) + min;
							}
    			</script>

        @endpush
        </div>
</x-app-layout>                