<x-app-layout>
	
    @push('breadcrumb')
    <h1>
        Dashboard
        <small>summary</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="#">My Dashboard</a></li>
      </ol>
      @endpush
	
   @if(Session::has('success'))
          <div class="alert alert-success text-center h4">
              {{ Session::get('success') }}
          </div>
          @endif

<div class="box bg-black bg-black bg-hexagons-dark">
    <div class="box-body">
      <h3 class="mt-0">My Finance Summery</h3>
    </div>
</div>
      <div class="row">
        <div class="col-xl-4 col-md-6 col-12">
              <!-- Default box -->
              <div class="box pull-up">
                <div class="box-body bg-yellow bg-hexagons-dark">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-200 font-size-30 text-center">${{ $user['balance']['balance'] }}</h4>
                        <p class="h2 text-center">BALANCE</p>

                      </div>
                    </div>
                </div>
              </div>
        </div>
        <div class="col-xl-4 col-md-6 col-12">
              <!-- Default box -->
              <div class="box pull-up">
                <div class="box-body bg-hexagons-dark">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center">${{ $user['balance']['pending'] }}</h4>
                        <p class="h2 text-center">PENDING</p>

                      </div>
                    </div>
                </div>
              </div>
        </div>
        <div class="col-xl-4 col-md-6 col-12">
              @if($user['balance']['direct'] > 0)
              <a href="{{ route('moveToBal',[app()->getLocale(),'direct']) }}" onclick="return confirm('Are you sure?')">
                @endif
              <div class="box pull-up">
                <div class="box-body bg-hexagons-dark">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center">${{ $user['balance']['direct'] }}</h4>
                        <p class="h2 text-center">DIRECT</p>
                      </div>
                    </div>
                </div>
              </div>
              @if($user['balance']['direct'] > 0)
            </a>
            @endif
        </div>
        <div class="col-xl-4 col-md-6 col-12">
              @if($user['balance']['network'] > 0)
              <a href="{{ route('moveToBal',[app()->getLocale(),'network']) }}" onclick="return confirm('Are you sure?')">
                @endif
              <div class="box pull-up">
                <div class="box-body bg-hexagons-dark">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center">${{ $user['balance']['network'] }}</h4>
                        <p class="h2 text-center">BINARY</p>
                      </div>
                    </div>
                </div>
              </div>
              @if($user['balance']['network'] > 0)
            </a>
            @endif
        </div>
        <div class="col-xl-4 col-md-6 col-12">
              @if($user['balance']['invest'] > 0)
              <a href="{{ route('moveToBal',[app()->getLocale(),'invest']) }}" onclick="return confirm('Are you sure?')">
                @endif
              <div class="box pull-up">
                <div class="box-body bg-hexagons-dark">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center">${{ $user['balance']['invest'] }}</h4>
                        <p class="h2 text-center">INVEST</p>
                      </div>
                    </div>
                </div>
              </div>
              @if($user['balance']['invest'] > 0)
            </a>
            @endif
        </div>
        <div class="col-xl-4 col-md-6 col-12">
              @if($user['balance']['transfer'] > 0)
              <a href="{{ route('moveToBal',[app()->getLocale(),'transfer']) }}" onclick="return confirm('Are you sure?')">
                @endif
              <div class="box pull-up">
                <div class="box-body bg-hexagons-dark">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center">${{ $user['balance']['transfer'] }}</h4>
                        <p class="h2 text-center">TRANSFER</p>
                      </div>
                    </div>
                </div>
              </div>
              @if($user['balance']['transfer'] > 0)
            </a>
            @endif
        </div>
    </div>

<div class="box bg-black bg-hexagons-dark">
    <div class="box-body">
      <h3 class="mt-0">My Account Summery</h3>
    </div>
</div>

@if(isset($user['pkgs']))

    <div class="row">
      <div class="col-12">
      <h1 class="page-header text-center">
        <span class="text-bold">Basic</span> account <span class="text-bold">information</span>
      </h1>
      </div>
        <div class="col-4 col-md-4 col-lg-4 col-xl-4">
             <div class="box text-center p-50 box-inverse bg-hexagons-dark pull-up" style="background-color:{{$user['pkgs']['color']}}">
              <div class="box-body">
                <h3 class="font-weight-50 font-size-30">{{ $user['pkgs']['name'] }} package</h3>
                <hr style="margin: 2.2rem auto;">
                <p><strong>+{{ $user['pkgs']['price'] }}</strong> Zooms</p>
                <p><strong>+{{ $user['pkgs']['invest'] }}% :  +{{ $user['pkgs']['invest']+10 }}%</strong> Zooms profit</p>
                <p><strong>{{ $user['pkgs']['invest_count'] }}%</strong> Profit without Zooms</p>
                <p><strong>{{ $user['pkgs']['invest_count']+$user['pkgs']['invest'] }}% : {{ $user['pkgs']['invest_count']+$user['pkgs']['invest']+10 }}%</strong> TOTAL Profit</p>
                <p><strong>{{ $user['pkgs']['max_out'] }}</strong> Max-Out</p>
                
                <h4>valid till : {{ $user->sub_end }}</h4>
              </div>
            </div>
          </div>

           <div class="col-4 col-md-4 col-lg-4 col-xl-4">
          <!-- Chart -->
          <div class="box bg-hexagons-dark">
            
            <div class="box-body">
              <div class="chart">
                <div id="e_chart_8" class="" style="height:400px;"></div> 
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-4 col-md-4 col-lg-4 col-xl-4">
            <div class="box box-body bg-hexagons-dark pull-up">
              <div class="flexbox align-items-center">
                <label class="toggler toggler-yellow">
                  <input type="checkbox" checked="">
                  <i class="fa fa-star"></i>
                </label>
                <div class="dropdown">
                  <a data-toggle="dropdown" href="#" aria-expanded="false"><i class="ion-android-more-vertical"></i></a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    <a class="dropdown-item" href="#"><i class="fa fa-fw fa-comments"></i> Messages</a>
                    <a class="dropdown-item" href="#"><i class="fa fa-fw fa-phone"></i> Call</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><i class="fa fa-fw fa-remove"></i> Remove</a>
                  </div>
                </div>
              </div>
              <div class="pt-3">
                @if($user['appline']['profile_photo_path'])
                  <img class="rounded my-10" src="{{ asset($user['appline']['profile_photo_path']) }}" alt="{{ $user['appline']['username'] }}">
                  @else
                  <img class="rounded my-10" src="{{ asset('img/default/user.png') }}" alt="{{ $user['appline']['username'] }}">
                  @endif
                <h3 class="m-0"><a href="#">{{ $user['appline']['username'] }}</a></h3>
                <hr style="margin: 1.6rem auto;">
                <span>your appline</span>
        
        
              </div>
            </div>
          </div>
     
    </div>
@else
    <div class="row">
        @foreach($pkgs as $pkg)
              <div class="col-lg-4">
                <div class="box text-center p-50 box-inverse bg-hexagons-dark pull-up" style="background-color:{{$pkg->color}}">
                  <div class="box-body">
                    <h5 class="text-uppercase">{{ $pkg->name }}</h5>
                    <br>
                    <h3 class="font-weight-100 font-size-70">${{ $pkg->price }}</h3>
                    <small>user / 25 month</small>

                    <hr>

                    <p><strong>+{{ $pkg->price }}</strong> Zooms</p>
                    <p><strong>+{{ $pkg->invest }}% :  +{{ $pkg->invest+10 }}%</strong> Zooms profit</p>
                    <p><strong>{{ $pkg->invest_count }}%</strong> Profit without Zooms</p>
                    <p><strong>{{ $pkg->invest_count+$pkg->invest }}% : {{ $pkg->invest_count+$pkg->invest+10 }}%</strong> TOTAL Profit</p>
                    <p><strong>{{ $pkg->max_out }}</strong> Max-Out</p>
                    @if($pkg->support == 1)
                    <p><strong>24x7</strong> Support</p>
                    @else
                    <p><strong>non</strong> Support</p>
                    @endif
                    <br><br>
                    <a class="btn btn-bold btn-block btn-outline btn-light" href="{{ route('buy_pkg',[app()->getLocale(),$pkg->id])}}">Buy plan</a>
                  </div>
                </div>
              </div>
        @endforeach
      
    </div>
@endif


@if(isset($user['pkgs']))
<div class="box bg-black bg-hexagons-dark">
    <div class="box-body">
      <h3><span class="text-bold">My</span> Network <span class="text-bold">Stat</span> with <span class="text-bold">Counts</span></h3>
    </div>
</div>

<div class="row">
            
            <div class="col-xl-3 col-md-6 col-12 ">
                <div class="box box-body pull-up bg-primary bg-hexagons-dark">
                  <div class="flexbox">
                    <span class="ion ion-ios-person-outline font-size-50"></span>
                    <span class="font-size-40 font-weight-200">{{$user->l}}</span>
                  </div>
                  <div class="text-right">Users Left</div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
              <div class="box box-body pull-up text-center bg-info bg-hexagons-dark">
                  <div class="font-size-40 font-weight-200">+{{$user->bvl}}</div>
                  <div>Zoms Left</div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
              <div class="box box-body pull-up text-center bg-info bg-hexagons-dark">
                  <div class="font-size-40 font-weight-200">+{{$user->bvr}}</div>
                  <div>Zoms Right</div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12 ">
                <div class="box box-body pull-up bg-primary bg-hexagons-dark">
                  <div class="flexbox">
                    <span class="font-size-40 font-weight-200">{{$user->r}}</span>
                    <span class="ion ion-ios-person-outline font-size-50"></span>
                  </div>
                  <div>Users Right</div>
                </div>
            </div>

        </div>
  @endif


@if(isset($user['invests'][0]['id']))
<div class="box bg-black bg-hexagons-dark">
    <div class="box-body">
      <h3><span class="text-bold">My</span> INVEST <span class="text-bold">Plans</span> with <span class="text-bold">ZOOMS</span></h3>
    </div>
</div>
<div class="row">
  
    @foreach($user['invests'] as $invest)
    <div class="col-xl-3 col-md-6 col-12 ">
            <div class="box box-body pull-up bg-info bg-hexagons-white">
              <div class="font-size-18 flexbox align-items-center">
                <span><img src="{{ asset('img/default/flag/'.strtolower($invest->flag).'.svg') }}" width="20"> {{ $invest['plans']['title'] }}</span>
                <i class="cc {{ $invest['plans']['sign'] }}-alt mr-5" title="{{ $invest['plans']['sign'] }}"></i>
                
              </div>
              
              <div class="progress progress-xxs mt-10 mb-0">
                <div class="progress-bar bg-danger" role="progressbar" style="width: {{ intval(Carbon\Carbon::now()->diffInDays($invest->next_round, false) * 100 / 30) }}%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <br>
              <small class="font-weight-300 mb-5">in {{ Carbon\Carbon::now()->diffInDays($invest->next_round, false) }} days</small>
              <small>almost {{ intval(Carbon\Carbon::now()->diffInHours($invest->next_round, false) ) }} Hours remain</small>
            </div>
        </div>
        @endforeach
  @else
  <div class="box bg-black bg-hexagons-dark">
    <div class="box-body">
      <h3 class="mt-0">My Invest Options</h3>
    </div>
</div>

  <div class="row">
    @foreach($plans as $plan)

      <div class="col-3">
            <div class="box ">
                <img class="card-img-top img-responsive" src="{{ asset($plan->image) }}" alt="Card image cap">

              <div class="box-body bg-hexagons-dark">
                <h4><a class="hover-info" href="#">{{$plan->title}}</a></h4>
                <p><span>{{$plan->start}}</span></p>

                <div class="flexbox align-items-center mt-3">
                  <a class="btn btn-bold btn-info" href="#">Read more</a>

                  <div class="gap-items-4">
                    <i class="cc {{ $plan->sign }} mr-5" title="{{ $plan->sign }}"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
    @endforeach
    </div>
  @endif
</div>

@if(isset($user['pkgs']))
    @push('scripts')
      <!-- EChartJS JavaScript -->
      <script src="{{ asset('assets/vendor_components/echarts-master/dist/echarts-en.min.js') }}"></script>
      <script src="{{ asset('assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js') }}"></script>
      
      <!-- Crypto_Admin for Chart purposes -->
      <script type="text/javascript">
        var dom = document.getElementById("e_chart_8");
        var myChart = echarts.init(dom);
        var app = {};
        option = null;
        option = {
            tooltip : {
                formatter: "{a} <br/>{b} : {c}%"
            },
          textStyle: {
            color: '#fff'
          },
            toolbox: {
                feature: {
                    restore: {},
                    saveAsImage: {}
                }
            },
            series: [
                {
                    name: 'Business indicators',
                    type: 'gauge',
                    detail: {formatter:'{value}%'},
                    data: [{value: 20, name: 'ZOOMS rate'}]
                }
            ]
        };

        setInterval(function () {
            option.series[0].data[0].value = (Math.random() * {{$user['pkgs']['invest']}} + 14).toFixed(2) - 0;
            myChart.setOption(option, true);
        },2000);
        ;
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }

      </script>
    @endpush
  @endif
</x-app-layout>                