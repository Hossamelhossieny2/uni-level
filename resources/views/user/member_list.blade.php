<x-app-layout>

     @push('breadcrumb')
    <h1>
        Members
        <small>genealogy List</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Members List</li>
      </ol>
      @endpush
    	
		<div class="row">
          <div class="col-12">
            <div class="box">
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example5" class="table table-hover">
					<thead class="d-none">
						<tr>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/1.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/2.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/4.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/5.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/6.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/7.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/8.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/9.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/1.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/2.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/3.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/4.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/5.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/6.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/7.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">dummy@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td class="w-20"><i class="fa fa-square-o pt-15"></i></td>
							<td class="w-20"><i class="fa fa-star text-yellow pt-15"></i></td>
							<td class="w-60">
							  <a class="avatar avatar-lg status-success" href="#">
								<img src="../../../images/avatar/8.jpg" alt="...">
							  </a>
							</td>
							<td class="w-300">
								<p class="mb-0">
								  <a href="#"><strong>Maryam Amiri</strong></a>
								  <small class="sidetitle">mike.stones@gmail.com</small>
								</p>
								<p class="mb-0">Designer</p>
							</td>
							<td>
								<nav class="nav mt-2">
								  <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
								  <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
								</nav>
							</td>
						</tr>
						
					</tbody>
				  </table>
				</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          </div>			
        </div>

</x-app-layout>