 <x-app-layout>
	
    @push('styles')
        	<!--alerts CSS -->
    <link href="{{ asset('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    @endpush
    @push('breadcrumb')
    <h1>
        Dashboard
        <small>General information</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
      @endpush

 <!-- Validation wizard -->
      <div class="box box-solid bg-dark">
        <div class="box-header with-border">
          <h3 class="box-title">Step wizard with validation</h3>
          <h6 class="box-subtitle">You can us the validation like what we did</h6>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body wizard-content">
			<form action="#" class="validation-wizard wizard-circle">
				<!-- Step 1 -->
				<h6>Step 1</h6>
				<section class="bg-hexagons-dark">
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="wlocation2"> Select City : <span class="danger">*</span> </label>
								<select class="custom-select form-control required" id="wlocation2" name="location">
									<option value="">Select City</option>
									<option value="India">India</option>
									<option value="USA">USA</option>
									<option value="Dubai">Dubai</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="wlocation2"> Select City : <span class="danger">*</span> </label>
								<select class="custom-select form-control required" id="wlocation2" name="location">
									<option value="">Select City</option>
									<option value="India">India</option>
									<option value="USA">USA</option>
									<option value="Dubai">Dubai</option>
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						
						<div class="col-md-6">
							<div class="form-group">
								<label for="wfirstName2"> Transaction password : <span class="danger">*</span> </label>
								<input type="text" class="form-control required" id="wfirstName2" name="firstName"> </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="wlastName2"> repeat password : <span class="danger">*</span> </label>
								<input type="text" class="form-control required" id="wlastName2" name="lastName"> </div>
						</div>
					</div>

				</section>
				<!-- Step 2 -->
				<h6>Step 2</h6>
				<section class="bg-hexagons-dark">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
		                  <label for="exampleInputFile">File input</label>
		                  <input type="file" id="exampleInputFile">

		                  <p class="help-block text-red">Example block-level help text here.</p>
		                </div>
						</div>
						<div class="col-md-6">
							
						</div>
						
					</div>
				</section>
				
				<!-- Step 4 -->
				<h6>Step 3</h6>
				<section class="bg-hexagons-dark">
					<div class="row">
						<div class="box">
			              <div class="card-body bg-img py-70" style="background-image: url(../../../images/gallery/thumb/5.jpg)" data-overlay="5">
			                <blockquote class="blockquote blockquote-inverse no-border no-margin">
			                  <p class="font-size-22">Integer efficitur nunc in blandit maximus.</p>
			                  <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
			                </blockquote>
			              </div>

			              <div class="box-body">
			                <h4><a href="#">Nullam id arcu dictum, volutpat dui nec</a></h4>
			                <p><span>April 28, 2022</span></p>

			                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>

			                <div class="flexbox align-items-center mt-3">
			                  <a class="btn btn-bold btn-info" href="#">Read more</a>

			                  <div class="gap-items-4">
			                    <a class="text-muted" href="#">
			                      <i class="fa fa-heart mr-1"></i> 12
			                    </a>
			                    <a class="text-muted" href="#">
			                      <i class="fa fa-comment mr-1"></i> 3
			                    </a>
			                  </div>
			                </div>
			              </div>
			            </div>
					</div>
				</section>
			</form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
		

       @push('scripts')
            <!-- steps  -->
					<script src="{{ asset('assets/vendor_components/jquery-steps-master/build/jquery.steps.js') }}"></script>
				   
				   <!-- validate  -->
				    <script src="{{ asset('assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js') }}"></script>
					
					<!-- Sweet-Alert  -->
				    <script src="{{ asset('assets/vendor_components/sweetalert/sweetalert.min.js') }}"></script>
				    
				    <!-- wizard  -->
				    <script src="{{ asset('js/pages/steps.js') }}"></script>
    
        @endpush
</x-app-layout>                