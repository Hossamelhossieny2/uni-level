 <x-app-layout>
	
    @push('styles')
        	<!--alerts CSS -->
    <link href="{{ asset('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    @endpush
    @push('breadcrumb')
    <h1>
        Dashboard
        <small>General information</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
      @endpush

@if(Session::has('success'))
        <div class="alert alert-success text-center h3">
            {{ Session::get('success') }}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger text-center h3">
            {{ Session::get('error') }}
        </div>
        @endif
        

<section class="content">
	<form role="form" method="post" class="form-element" action="{{ route('storejoinApp',app()->getLocale()) }}">
		@csrf
     <div class="row">
        <div class="col-xl-3 col-md-6 col-12">
              <!-- Default box -->
              <div class="box pull-up">
                <div class="box-body bg-hexagons-white">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center @if($user['balance']['balance'] == 0) text-red @endif">${{ $user['balance']['balance'] }}</h4>
                        <p class="h2 text-center">BALANCE</p>
                      </div>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->                
        </div>
        <div class="col-xl-3 col-md-6 col-12">
              <!-- Default box -->
              <div class="box pull-up">
                <div class="box-body bg-hexagons-white">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center @if($user['balance']['direct'] == 0) text-red @endif"">${{ $user['balance']['direct'] }}</h4>
                        <p class="h2 text-center">DIRECT</p>
                      </div>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->                
        </div>
        <div class="col-xl-3 col-md-6 col-12">
              <!-- Default box -->
              <div class="box pull-up">
                <div class="box-body bg-hexagons-white">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center @if($user['balance']['network'] == 0) text-red @endif"">${{ $user['balance']['network'] }}</h4>
                        <p class="h2 text-center">NETWORK</p>
                      </div>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->                
        </div>
        <div class="col-xl-3 col-md-6 col-12">
              <!-- Default box -->
              <div class="box pull-up">
                <div class="box-body bg-hexagons-white">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="font-weight-100 font-size-30 text-center @if($user['balance']['transfer'] == 0) text-red @endif"">${{ $user['balance']['transfer'] }}</h4>
                        <p class="h2 text-center">TRANSFER</p>
                      </div>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->                
        </div>
    </div>

      <div class="row">

		<div class="col-4 col-md-4 col-xl-6 col-12">
			<a class="box box-link-pop text-center" href="javascript:void(0)">
				<div class="box-body">
					<p class="font-size-40" style="color:{{$pkg->color}};">
						<strong><i class="cc USDT" title="USDT"></i>{{ $pkg->price }}</strong>
					</p>
				</div>
				<div class="box-body py-35 bg-light">
					<p class="font-weight-600">
						<i class="fa fa-money text-muted mr-5"></i> price
					</p>
				</div>
			</a>
		</div>

		 <div class="col-4 col-md-4 col-xl-6 col-12">
			<a class="box box-link-shadow text-center pull-up" href="javascript:void(0)">
				<div class="box-body" >
					<p class="mt-5">
						<i class="icon-diamond fa-3x" style="color:{{$pkg->color}};"></i>
					</p>
				</div>
				<div class="box-body py-35 bg-light">
					<p class="font-weight-600">{{$pkg->name}} plan</p>
				</div>
			</a>
		</div>

		<div class="col-4 col-md-4 col-xl-6 col-12">
			<a class="box box-link-shadow text-center pull-up" href="javascript:void(0)">
				<div class="box-body" >
					<p class="mt-5">
						<i class="fa fa-address-card-o fa-3x" aria-hidden="true" style="color:{{$pkg->color}};"></i>
					</p>
				</div>
				<div class="box-body py-35 bg-light">
					<div class="form-group">
						
                  <input type="text" class="form-control" name="usertoken" placeholder="appline link" required="">
                </div>
				</div>
			</a>
		</div>

		<div class="col-4 col-md-4 col-xl-6 col-12">
			<a class="box box-link-shadow text-center pull-up" href="javascript:void(0)">
				<div class="box-body" >
					<p class="mt-5">
						<i class="fa fa-unlock fa-3x" aria-hidden="true" style="color:{{$pkg->color}};"></i>
					</p>
				</div>
				<div class="box-body py-35 bg-light">
					<div class="form-group">
                  <input type="password" class="form-control" name="trans_pass" placeholder="transaction password" required="">
                </div>
				</div>
			</a>
		</div>

	</div>

	<input type="hidden" name="pkg" value="{{$pkg->id}}">

	<div class="row">
		<div class="col-12">
		  @if($user['balance']['balance'] >= $pkg->price)
		  	<button type="submit" class="btn btn-block btn-success">confirm Buy this package</button>
		  @else
		  	@if(!empty($wallets))
		  		<a href="{{ route('charge_request',app()->getLocale()) }}" type="button" class="btn btn-block btn-danger">charge your wallet now</a>
			@else
				<a href="{{ route('my_wallet',app()->getLocale()) }}" type="button" class="btn btn-block btn-danger">add wallet first To charge</a>
			@endif
		  @endif
		</div>
	</div>
</form>
</div> 
		

       @push('scripts')
            <!-- steps  -->
					<script src="{{ asset('assets/vendor_components/jquery-steps-master/build/jquery.steps.js') }}"></script>
				   
				   <!-- validate  -->
				    <script src="{{ asset('assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js') }}"></script>
					
					<!-- Sweet-Alert  -->
				    <script src="{{ asset('assets/vendor_components/sweetalert/sweetalert.min.js') }}"></script>
				    
				    <!-- wizard  -->
				    <script src="{{ asset('js/pages/steps.js') }}"></script>
    
        @endpush
</x-app-layout>                