<x-app-layout>

@push('breadcrumb')
    <h1>
        Dashboard
        <small>General information</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
      @endpush

      @foreach ($errors->all() as $error)
                <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
            @endforeach

@if(!isset(auth()->user()->trans_pass))
<div class="register-box">

  <div class="register-box-body">
	  <div class="register-logo">
		<a href="#"><b>Secure</b>Page</a>
	  </div>
    <p class="login-box-msg">Transactions Password Needed</p>

    <form action="{{ route('add_trans_pass',app()->getLocale()) }}" method="post" class="form-element">
      @csrf
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Transactions Password" name="password">
        <span class="ion ion-locked form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype Transactions password" name="password_confirmation">
        <span class="ion ion-log-in form-control-feedback "></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="submit" class="btn btn-info btn-block margin-top-10">Add Trans Password</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
	
	
	<!-- /.social-auth-links -->
    
     <div class="margin-top-20 text-center">
    	<p>Forget My Transaction Password ?<a href="#" class="text-info m-l-5"> Notify Me !</a></p>
     </div>
    
  </div>
  <!-- /.form-box -->
</div>
@else

<div class="col-12">
        <!-- Default box -->
        <div class="box pull-up">
        <div class="box-body bg-hexagons-white">
            <div class="media flex-column text-center p-40">
            <span class="avatar avatar-xxl bg-success opacity-60 mx-auto">
            <i class="align-sub ion ion-checkmark font-size-40"></i>
            </span>
            <div class="mt-20">
              <h6 class="text-uppercase fw-500">Transaction Password Added !</h6>
              <small>Now You Can Navigate in your Wallet Securlly.</small>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->        
      </div>

@endif
<!-- /.register-box -->
</x-app-layout>