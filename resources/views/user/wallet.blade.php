<x-app-layout>

<style type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css"></style>
<style type="text/css" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap5.min.css"></style>

<div class="max-w-6xl mx-auto sm:px-6 lg:px-8 bg-white mt-10">


			<table id="example" class="table table-striped cell-border" style="width:100%">
	        <thead>
	            <tr>
	                <th>transaction</th>
	                <th style="width:30%">description</th>
	                <th>in</th>
	                <th>out</th>
	                <th>date</th>
	                <th>note</th>
	            </tr>
	        </thead>
	        <tbody>

	            <tr>
	                <td>I-001</td>
	                <td>initial balance</td>
	                <td>0,00</td>
	                <td>0,00</td>
	                <td>2020/09/25</td>
	                <td>-</td>
	            </tr>
	           
	        </tbody>
	        <tfoot>
	            <tr>
	                <th>transaction</th>
	                <th>description</th>
	                <th>in</th>
	                <th>out</th>
	                <th>date</th>
	                <th>note</th>
	            </tr>
	        </tfoot>
	    </table>

</div>

	    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	    <script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
	    <script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap5.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
			    $('#example').DataTable();
			} );
		</script>

</x-app-layout>