<x-app-layout>
	
    @push('styles')
        <!--amcharts -->
        <link href="https://www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
    @endpush
    @push('breadcrumb')
    <h1>
        Dashboard
        <small>General information</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
      @endpush
	

    <div class="row">
           
          <!-- pool list https://raw.githubusercontent.com/blockchain/Blockchain-Known-Pools/master/pools.json -->

            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">Intra-day Data</h4>

                      <ul class="box-controls pull-right">
                          <li><a class="box-btn-close" href="#"></a></li>
                          <li><a class="box-btn-slide"  href="#"></a></li>  
                          <li><a class="box-btn-fullscreen" href="#"></a></li>
                      </ul>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <div id="chartdiv21" style="height: 500px;"></div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="box pull-up">
                  <div class="box-body">
                      <div class="media align-items-center p-0">
                        <div class="text-center">
                          <a href="#"><i class="cc BTC mr-5" title="BTC"></i></a>
                        </div>
                        <div>
                          <h3 class="no-margin text-bold">Bitcoin BTC</h3>
                        </div>
                      </div>
                      <div class="flexbox align-items-center mt-5">
                        <div>
                          <p class="no-margin font-weight-600"><span class="text-yellow">0.00000434 </span>BTC<span class="text-info">$0.04</span></p>
                        </div>
                        <div class="text-right">
                          <p class="no-margin font-weight-600"><span class="text-success">+1.35%</span></p>
                        </div>
                      </div>
                </div>
                <div class="box-footer p-0 no-border">
                    <div class="chart"><canvas id="chartjs1" class="h-80"></canvas></div>
                </div>
             </div>
          </div>
            <div class="col-lg-3 col-md-6">
                <div class="box pull-up">
                  <div class="box-body">
                      <div class="media align-items-center p-0">
                        <div class="text-center">
                          <a href="#"><i class="cc LTC mr-5" title="LTC"></i></a>
                        </div>
                        <div>
                          <h3 class="no-margin text-bold">Litecoin LTC</h3>
                        </div>
                      </div>
                      <div class="flexbox align-items-center mt-5">
                        <div>
                          <p class="no-margin font-weight-600"><span class="text-yellow">0.00000434 </span>LTC<span class="text-info">$0.04</span></p>
                        </div>
                        <div class="text-right">
                          <p class="no-margin font-weight-600"><span class="text-danger">-1.35%</span></p>
                        </div>
                      </div>
                </div>
                <div class="box-footer p-0 no-border">
                    <div class="chart"><canvas id="chartjs2" class="h-80"></canvas></div>
                </div>
             </div>
          </div>
            <div class="col-lg-3 col-md-6">
                <div class="box pull-up">
                  <div class="box-body">
                      <div class="media align-items-center p-0">
                        <div class="text-center">
                          <a href="#"><i class="cc NEO mr-5" title="NEO"></i></a>
                        </div>
                        <div>
                          <h3 class="no-margin text-bold">Neo NEO</h3>
                        </div>
                      </div>
                      <div class="flexbox align-items-center mt-5">
                        <div>
                          <p class="no-margin font-weight-600"><span class="text-yellow">0.00000434 </span>NEO<span class="text-info">$0.04</span></p>
                        </div>
                        <div class="text-right">
                          <p class="no-margin font-weight-600"><span class="text-danger">-1.35%</span></p>
                        </div>
                      </div>
                </div>
                <div class="box-footer p-0 no-border">
                    <div class="chart"><canvas id="chartjs3" class="h-80"></canvas></div>
                </div>
             </div>
          </div>
            <div class="col-lg-3 col-md-6">
                <div class="box pull-up">
                  <div class="box-body">
                      <div class="media align-items-center p-0">
                        <div class="text-center">
                          <a href="#"><i class="cc DASH mr-5" title="DASH"></i></a>
                        </div>
                        <div>
                          <h3 class="no-margin text-bold">Dash DASH</h3>
                        </div>
                      </div>
                      <div class="flexbox align-items-center mt-5">
                        <div>
                          <p class="no-margin font-weight-600"><span class="text-yellow">0.00000434 </span>DASH<span class="text-info">$0.04</span></p>
                        </div>
                        <div class="text-right">
                          <p class="no-margin font-weight-600"><span class="text-success">+1.35%</span></p>
                        </div>
                      </div>
                </div>
                <div class="box-footer p-0 no-border">
                    <div class="chart"><canvas id="chartjs4" class="h-80"></canvas></div>
                </div>
             </div>
          </div>
                
            <div class="col-lg-7 col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">Recent Trading Activities</h4>
                        <ul class="box-controls pull-right">
                          <li><a class="box-btn-close" href="#"></a></li>
                          <li><a class="box-btn-slide" href="#"></a></li>   
                          <li><a class="box-btn-fullscreen" href="#"></a></li>
                        </ul>
                    </div>
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table no-bordered no-margin table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Trade Time</th>
                                        <th>Status</th>
                                        <th>Last Trade</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>#12457</th>
                                        <td>11.00AM</td>
                                        <td><span class="label label-success">Complete</span></td>
                                        <td><i class="fa fa-arrow-down text-danger"></i> 0.124587 BTC</td>
                                    </tr>
                                    <tr>
                                        <th>#12586</th>
                                        <td>10.11AM</td>
                                        <td><span class="label label-danger">Pending</span></td>
                                        <td><i class="fa fa-arrow-down text-danger"></i> 3.84572 LTC</td>
                                    </tr>
                                    <tr>
                                        <th>#13258</th>
                                        <td>09.12AM</td>
                                        <td><span class="label label-danger">Pending</span></td>
                                        <td><i class="fa fa-arrow-up text-success"></i> 0.215485 LTC</td>
                                    </tr>
                                    <tr>
                                        <th>#13586</th>
                                        <td>08.22AM</td>
                                        <td><span class="label label-warning">Cancelled</span></td>
                                        <td><i class="fa fa-arrow-down text-danger"></i> 0.8457952 BTC</td>
                                    </tr>
                                    <tr>
                                        <th>#14578</th>
                                        <td>07.48AM</td>
                                        <td><span class="label label-success">Complete</span></td>
                                        <td><i class="fa fa-arrow-up text-success"></i> 0.954278 DASH</td>
                                    </tr>
                                    <tr>
                                        <th>#15623</th>
                                        <td>06.45AM</td>
                                        <td><span class="label label-success">Complete</span></td>
                                        <td><i class="fa fa-arrow-up text-success"></i> 0.9654582 BTC</td>
                                    </tr>
                                    <tr>
                                        <th>#15685</th>
                                        <td>05.11PM</td>
                                        <td><span class="label label-warning">Cancelled</span></td>
                                        <td><i class="fa fa-arrow-down text-danger"></i> 9.8545269 LTC</td>
                                    </tr>
                                    <!-- Repeat -->
                                    <tr>
                                        <th>#16585</th>
                                        <td>23.18PM</td>
                                        <td><span class="label label-danger">Pending</span></td>
                                        <td><i class="fa fa-arrow-up text-success"></i> 1.9564258 DASH</td>
                                        </tr>
                                    <tr>
                                        <th>#16785</th>
                                        <td>19.27PM</td>
                                        <td><span class="label label-success">Complete</span></td>
                                        <td><i class="fa fa-arrow-down text-danger"></i> 12.845725 LTC</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>                      
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

              </div>
            
            <div class="col-lg-5 col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">Markets</h4>
                    </div>
                    <div class="box-body no-padding">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="navpills-1" class="tab-pane active">
                                <div class="table-responsive">
                                  <table class="table table-hover no-margin text-center">
                                      <thead>
                                        <tr>
                                          <th scope="col">Market</th>
                                          <th scope="col">Price</th>
                                          <th scope="col">Change</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>BTC - 12458</td>
                                          <td>0.002548548</td>
                                          <td>+12.85% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 02157</td>
                                          <td>0.025486854</td>
                                          <td>+05.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 12457</td>
                                          <td>0.025845218</td>
                                          <td>-02.01% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 35487</td>
                                          <td>0.021548754</td>
                                          <td>+06.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 03254</td>
                                          <td>0.025845845</td>
                                          <td>-07.09% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 12458</td>
                                          <td>0.002548548</td>
                                          <td>+12.85% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 15875</td>
                                          <td>0.2485975</td>
                                          <td>-08.15% <i class="fa fa-arrow-up text-danger"></i></td>
                                        </tr>
                                        <tr>
                                          <td>BTC - 12457</td>
                                          <td>0.025845218</td>
                                          <td>-02.01% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane">
                                <div class="table-responsive">
                                  <table class="table table-hover text-center">
                                      <thead>
                                        <tr>
                                          <th scope="col">Market</th>
                                          <th scope="col">Price</th>
                                          <th scope="col">Change</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>ETH - 12458</td>
                                          <td>0.002548548</td>
                                          <td>+12.85% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>ETH - 02157</td>
                                          <td>0.025486854</td>
                                          <td>+05.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>ETH - 12457</td>
                                          <td>0.025845218</td>
                                          <td>-02.01% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                        <tr>
                                          <td>ETH - 35487</td>
                                          <td>0.021548754</td>
                                          <td>+06.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>ETH - 03254</td>
                                          <td>0.025845845</td>
                                          <td>-07.09% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane">
                                <div class="table-responsive">
                                  <table class="table table-hover text-center">
                                      <thead>
                                        <tr>
                                          <th scope="col">Market</th>
                                          <th scope="col">Price</th>
                                          <th scope="col">Change</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>DASH - 12458</td>
                                          <td>0.002548548</td>
                                          <td>+12.85% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>DASH - 02157</td>
                                          <td>0.025486854</td>
                                          <td>+05.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>DASH - 12457</td>
                                          <td>0.025845218</td>
                                          <td>-02.01% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                        <tr>
                                          <td>DASH - 35487</td>
                                          <td>0.021548754</td>
                                          <td>+06.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>DASH - 03254</td>
                                          <td>0.025845845</td>
                                          <td>-07.09% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane">
                                <div class="table-responsive">
                                  <table class="table table-hover text-center">
                                      <thead>
                                        <tr>
                                          <th scope="col">Market</th>
                                          <th scope="col">Price</th>
                                          <th scope="col">Change</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>LTC - 12458</td>
                                          <td>0.002548548</td>
                                          <td>+12.85% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>LTC - 02157</td>
                                          <td>0.025486854</td>
                                          <td>+05.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>LTC - 12457</td>
                                          <td>0.025845218</td>
                                          <td>-02.01% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                        <tr>
                                          <td>LTC - 35487</td>
                                          <td>0.021548754</td>
                                          <td>+06.15% <i class="fa fa-arrow-up text-success"></i></td>
                                        </tr>
                                        <tr>
                                          <td>LTC - 03254</td>
                                          <td>0.025845845</td>
                                          <td>-07.09% <i class="fa fa-arrow-down text-danger"></i></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                      
                        <!-- Nav tabs -->
                        <ul class="nav nav-fill nav-pills margin-bottom margin-top-10">
                            <li class="nav-item bt-2 border-warning"> <a href="#navpills-1" class="nav-link active no-radius" data-toggle="tab" aria-expanded="false">BTC</a> </li>
                            <li class="nav-item bt-2 border-primary"> <a href="#navpills-2" class="nav-link no-radius" data-toggle="tab" aria-expanded="false">ETH</a> </li>
                            <li class="nav-item bt-2 border-success"> <a href="#navpills-3" class="nav-link no-radius" data-toggle="tab" aria-expanded="true">DASH</a> </li>
                            <li class="nav-item bt-2 border-danger"> <a href="#navpills-4" class="nav-link no-radius" data-toggle="tab" aria-expanded="true">LTC</a> </li>
                        </ul>                                               
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>          
        </div>  
        @push('scripts')
            <!-- webticker -->
            <script src="{{ asset('assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js') }}"></script>
    
            <!--amcharts charts -->
            <script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/gauge.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/amstock.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
            <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
            <script src="http://www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/themes/patterns.js" type="text/javascript"></script>
            <script src="http://www.amcharts.com/lib/3/themes/dark.js" type="text/javascript"></script> 
             <!-- Chart -->
            <script src="{{ asset('assets/vendor_components/chart.js-master/Chart.min.js') }}"></script>
            <script src="{{ asset('js/pages/chartjs-int.js') }}"></script>
            <!-- Crypto_Admin dashboard demo (This is only for demo purposes) -->
            <script src="{{ asset('js/pages/dashboard.js') }}"></script>
    
        @endpush
</x-app-layout>                