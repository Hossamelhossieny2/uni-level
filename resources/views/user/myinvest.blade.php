<x-app-layout>
	
    @push('styles')
      <!-- flipclock-->
    <link rel="stylesheet" href="{{ asset('assets/vendor_components/FlipClock-master/compiled/flipclock.css') }}">
    @endpush

    @push('breadcrumb')
    <h1>
        Invest
        <small>My Investment</small>
      </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard',app()->getLocale()) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
        <li class="breadcrumb-item active">My Invest</li>
      </ol>
      @endpush
	
   <div class="row">
    <div class="col-12">
      <h1 class="page-header text-center">
        <span class="text-bold">My</span> Latest <span class="text-bold">invest</span> with <span class="text-bold">greenway</span><br>
        <p class="font-size-18 mb-0">With ZOOMS extra bonus .</p>
      </h1>
    </div>
  
  @foreach($user['invests'] as $invest)
  <input type="hidden" id="next{{$invest->id}}" value="{{$invest->next_round}}">
    <div class="row">
    <div class="col-8">
      <div class="box box-dark bg-hexagons-white">
      <div class="box-body">
        <h5 class="text-white text-center">Distribution Ends In:</h5>
        <div class="countdownv2_holder text-center mb-50 mt-20">
          <div id="clock{{$invest->id}}"></div>
          </div>
        
      </div>
      <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>   
    

    <div class="col-4">
          <div class="box box-default pull-up">
            <div class="box-body">     
            <h4 class="box-title"><i class="cc {{ $invest['plans']['sign'] }} mr-5" title="{{ $invest['plans']['sign'] }}"></i>         
              {{ $invest['plans']['title'] }}</h4>
              <p class="box-text">
                <div class="font-size-20 text-primary">Distribution Date:  <span id="t{{$invest->id}}" class="text-bold">{{ Carbon\Carbon::parse($invest->next_round)->format('d M') }}</span></div>
                <div class="font-size-20 text-yellow">Excpeted Value:  <span class="text-bold">${{ $invest->gain_round }}</span></div>
              </p>
              
              <p class="box-text">{{ $invest['plans']['desc'] }}</p>
              <img src="{{ asset('img/default/flag/'.strtolower($invest->flag).'.svg') }}" width="40">
              <a href="#" class="btn btn-blue">{{ $invest->amount }} $</a>
              <a href="#" class="btn btn-success">24 month</a>
          </div>
        </div>    
    </div>   
    </div> 
@endforeach

    @push('scripts')
     <!-- FastClick -->
  <script src="{{ asset('assets/vendor_components/FlipClock-master/compiled/flipclock.min.js') }}"></script>
    <script type="text/javascript">
    $(function () {
       'use strict';
      const numbers = [{{$inv_ids}}];
      const hours = [{{$inv_hrs}}];
      numbers.forEach(function(number) {
          var eventD = document.getElementById("next"+number).value;
          var eventDate = new Date(eventD);
          console.log(eventDate);
          var clock = $('#clock'+number).FlipClock(eventDate, {
            clockFace: 'DailyCounter',
            countdown: true,
            autoStart: true
          });
      });
      
    });
  </script>
    
  
      
    @endpush
            
</x-app-layout>                