<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('storejoinApp',app()->getLocale()) }}">
            @csrf

			<div class="mt-4">
                <x-jet-label for="usertoken" value="{{ __('Token') }}" />
                <x-jet-input id="usertoken" class="block mt-1 w-full" type="text" name="usertoken" value="{{$usertoken}}" />
            </div>

            <div class="mt-4">
                <x-jet-label for="pkg" value="{{ __('pkg') }}" />
                <x-jet-input id="pkg" class="block mt-1 w-full" type="text" name="pkg" value="{{$pkg}}" />
            </div>
            
            <div>
                <x-jet-label for="username" value="{{ __('UserName') }}" />
                <x-jet-input id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username')" autofocus autocomplete="username" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" />
            </div>

            <div class="fmt-4">
                           <label for="gender">{{ __('select_gender') }}</label>
                            <select id="gender" name="gender" class="block mt-1 w-full" required>
                                <option value="" >{{ __('select_gender') }}</option>
                                <option value="male" >{{ __('male') }}</option>
                                <option value="female">{{ __('female') }}</option>
                            </select>
                            @error('gender') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" autocomplete="new-password" />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-jet-label for="terms">
                        <div class="flex items-center">
                            <x-jet-checkbox name="terms" id="terms"/>

                            <div class="ml-2">
                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
